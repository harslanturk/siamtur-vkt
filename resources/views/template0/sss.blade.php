@extends('layouts.template0.master')
@section('content')

<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">

            <!-- Post Content
            ============================================= -->
            <div class="postcontent nobottommargin col_last clearfix">

                <div class="single-post nobottommargin">

                    <!-- Single Post
                    ============================================= -->
                    <div class="entry clearfix">

                        <!-- Entry Title
                        ============================================= -->
                        <div class="entry-title">
                            <h2>{{$data->title}}</h2>
                        </div><!-- .entry-title end -->

                        <!-- Entry Image
                        ============================================= -->
                        <div class="entry-image">
                            @if(!empty($data->page_k_img))
                            <img src="http://s1crm.siamtur.com{{$data->page_k_img}}" alt="Vietnam Kamboçya Tayland Turu">
                            @else
                            <img src="/img/template1/backimg.jpg" alt="Vietnam Kamboçya Tayland Turu">
                            @endif
                        </div><!-- .entry-image end -->

                        <!-- Entry Content
                        ============================================= -->
                        <div class="entry-content notopmargin">

                            <?=$data->content?>
                            <!-- Post Single - Content End -->

                            <!-- Tag Cloud
                            ============================================= -->
                            <div class="tagcloud clearfix bottommargin">
                                <a href="#">vietnam</a>
                                <a href="#">kamboçya</a>
                                <a href="#">laos</a>
                                <a href="#">sık sorulan sorular</a>
                                <a href="#">vietnam kamboçya turu</a>
                                <a href="#">vietnam turu</a>
                            </div><!-- .tagcloud end -->

                            <div class="clear"></div>

                            <!-- Post Single - Share
                            ============================================= -->
                            <div class="si-share noborder clearfix">
                                <span>Paylaş:</span>
                                <div>
                                    <a href="#" class="social-icon si-borderless si-facebook">
                                        <i class="icon-facebook"></i>
                                        <i class="icon-facebook"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-twitter">
                                        <i class="icon-twitter"></i>
                                        <i class="icon-twitter"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-pinterest">
                                        <i class="icon-pinterest"></i>
                                        <i class="icon-pinterest"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-gplus">
                                        <i class="icon-gplus"></i>
                                        <i class="icon-gplus"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-rss">
                                        <i class="icon-rss"></i>
                                        <i class="icon-rss"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-email3">
                                        <i class="icon-email3"></i>
                                        <i class="icon-email3"></i>
                                    </a>
                                </div>
                            </div><!-- Post Single - Share End -->

                        </div>
                    </div><!-- .entry end -->

                    <h4>Önerilen Turlar:</h4>

                    <div id="shop" class="shop product-3 grid-container clearfix">
                        @foreach($data2Tours as $value)
                        <div class="product clearfix">
                            <div class="product-image">
                                <a href="#"><img src="http://siamtur.com/s1/{{ $value->resim }}" alt="{{ $value->tur_ad }}" style="height: 160px;"></a>
                                <a href="#"><img src="http://siamtur.com/s1/{{ $value->resim }}" alt="{{ $value->tur_ad }}" style="height: 160px;"></a>
                                <div class="sale-flash">Erken Rezervasyon*</div>                            
                            </div>
                            <div class="product-desc center">
                                <div class="product-title"><h3><a href="#">{{ $value->tur_ad }}</a></h3></div>
                                <div class="product-title"><h3>{{ Helper::DateConvertTurkishDMY("$value->htarihi") }}</h3></div>
                                <div class="product-price"><del><?= $value->ana_fiyat + 150; ?> $</del> <ins>{{ $value->ana_fiyat }} $</ins></div>
                                <div class="product-rating">
                                    <i class="fa fa-credit-card"></i>
                                    <i class="fa fa-cc-visa"></i>
                                    <i class="fa-plane"></i>
                                    <i class="fa-car"></i>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div><!-- #shop end -->

                </div>

            </div><!-- .postcontent end -->

            <!-- Sidebar
            ============================================= -->
            <div class="sidebar nobottommargin clearfix">
                <div class="sidebar-widgets-wrap">

                    @include('template0.sidebar')                    

                    <!--<div class="widget clearfix">

                        <h4>Galeri</h4>
                        <div id="flickr-widget" class="flickr-feed masonry-thumbs" data-id="613394@N22" data-count="16" data-type="group" data-lightbox="gallery"></div>

                    </div>-->


                    <!--<div class="widget clearfix">

                        <h4>Reklam</h4>
                        <div id="oc-portfolio-sidebar" class="owl-carousel carousel-widget" data-items="1" data-margin="10" data-loop="true" data-nav="false" data-autoplay="5000">

                            <div class="oc-item">
                                <div class="iportfolio">
                                    <div class="portfolio-image">
                                        <a href="#">
                                            <img src="images/portfolio/4/3.jpg" alt="Mac Sunglasses">
                                        </a>
                                        <div class="portfolio-overlay">
                                            <a href="http://vimeo.com/89396394" class="center-icon" data-lightbox="iframe"><i class="icon-line-play"></i></a>
                                        </div>
                                    </div>
                                    <div class="portfolio-desc center nobottompadding">
                                        <h3><a href="portfolio-single-video.html">Mac Sunglasses</a></h3>
                                        <span><a href="#">Graphics</a>, <a href="#">UI Elements</a></span>
                                    </div>
                                </div>
                            </div>

                            <div class="oc-item">
                                <div class="iportfolio">
                                    <div class="portfolio-image">
                                        <a href="portfolio-single.html">
                                            <img src="images/portfolio/4/1.jpg" alt="Open Imagination">
                                        </a>
                                        <div class="portfolio-overlay">
                                            <a href="images/blog/full/1.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
                                        </div>
                                    </div>
                                    <div class="portfolio-desc center nobottompadding">
                                        <h3><a href="portfolio-single.html">Open Imagination</a></h3>
                                        <span><a href="#">Media</a>, <a href="#">Icons</a></span>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>-->


                </div>

            </div><!-- .sidebar end -->

        </div>

    </div>

</section><!-- #content end -->
<script>
        jQuery(document).ready( function($){
            $('#shop').isotope({
                transitionDuration: '0.65s',
                getSortData: {
                    name: '.product-title',
                    price_lh: function( itemElem ) {
                        if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
                            var price = $(itemElem).find('.product-price ins').text();
                        } else {
                            var price = $(itemElem).find('.product-price').text();
                        }

                        priceNum = price.split("$");

                        return parseFloat( priceNum[1] );
                    },
                    price_hl: function( itemElem ) {
                        if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
                            var price = $(itemElem).find('.product-price ins').text();
                        } else {
                            var price = $(itemElem).find('.product-price').text();
                        }

                        priceNum = price.split("$");

                        return parseFloat( priceNum[1] );
                    }
                },
                sortAscending: {
                    name: true,
                    price_lh: true,
                    price_hl: false
                }
            });

            $('.custom-filter:not(.no-count)').children('li:not(.widget-filter-reset)').each( function(){
                var element = $(this),
                    elementFilter = element.children('a').attr('data-filter'),
                    elementFilterContainer = element.parents('.custom-filter').attr('data-container');

                elementFilterCount = Number( jQuery(elementFilterContainer).find( elementFilter ).length );

                element.append('<span>'+ elementFilterCount +'</span>');

            });

            $('.shop-sorting li').click( function() {
                $('.shop-sorting').find('li').removeClass( 'active-filter' );
                $(this).addClass( 'active-filter' );
                var sortByValue = $(this).find('a').attr('data-sort-by');
                $('#shop').isotope({ sortBy: sortByValue });
                return false;
            });
        });
    </script>
@endsection
