@extends('layouts.template0.master')
@section('header-sector')
<title>{{$seo->sayfa_ad}}</title>
@endsection
@section('content')

    <!-- Page Title
		============================================= -->
    <section id="page-title">

        <div class="container clearfix">
            <h1>{{ $tour->tur_ad_gor }}</h1>
            <ol class="breadcrumb">
                <li><a href="#">Anasayfa</a></li>
                <li><a href="#">Turlarımız</a></li>
                <li class="active">{{ $tour->tur_ad_gor }}</li>
            </ol>
        </div>

    </section><!-- #page-title end -->

    <!-- Content
    ============================================= -->
    <section id="content">

        <div class="content-wrap">

            <div class="container clearfix">

                <div class="single-product">

                    <div class="product">

                        <div class="col_two_fifth">

                            <!-- Product Single - Gallery
                            ============================================= -->
                            <div class="product-image">
                                <div class="fslider" data-pagi="false" data-arrows="false" data-thumbs="true">
                                    <div class="flexslider">
                                        <div class="slider-wrap" data-lightbox="gallery">
                                            @foreach($fotoGaleri as $foto)
                                            <div class="slide" data-thumb="http://siamtur.com/s1/{{ $foto->resim }}"><a href="http://siamtur.com/s1/{{ $foto->resim }}" title="{{ $tour->tur_ad_gor }}" data-lightbox="gallery-item"><img src="http://siamtur.com/s1/{{ $foto->resim }}" alt="{{ $tour->tur_ad_gor }}"></a></div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="sale-flash">Son 2 Kişi!</div>
                            </div><!-- Product Single - Gallery End -->

                        </div>

                        <div class="col_two_fifth product-desc">

                            <!-- Product Single - Price
                            ============================================= -->
                            <div class="product-price"><del><?php $del= $tour->ana_fiyat+200; echo $del; ?> $</del> <ins>{{ $tour->ana_fiyat }} $</ins></div><!-- Product Single - Price End -->

                            <!-- Product Single - Rating
                            ============================================= -->
                            <!--<div class="product-rating">
                                <i class="icon-star3"></i>
                                <i class="icon-star3"></i>
                                <i class="icon-star3"></i>
                                <i class="icon-star-half-full"></i>
                                <i class="icon-star-empty"></i>
                            </div><!-- Product Single - Rating End -->

                            <div class="clear"></div>
                            <div class="line"></div>

                            <!-- Product Single - Price
                            ============================================= -->
                            <div class="product-price" style="color:#333; font-size:16px;">
                                <?php
                                    echo $tour->gun." Gün ".$tour->gece." Gece";
                                ?>
                            </div><!-- Product Single - Price End -->

                            <!-- Product Single - Rating
                            ============================================= -->
                            <div class="product-price" style="color:#333; float:right; font-size:16px;">
                                <?php
                                if($tour->htarihi=="0000-00-00" || $tour->htarihi==NULL)
                                {
                                    echo "HER GÜN HAREKETLİ";
                                }
                                else
                                {
                                    echo $tour->htarihi;
                                }
                                ?>
                            </div><!-- Product Single - Rating End -->

                            <div class="clear"></div>
                            <div class="line"></div>

                            <!-- Product Single - Quantity & Cart Button
                            ============================================= -->
                            <a href="#" class="button button-border button-rounded button-red"><i class="icon-mail"></i>Talep Et</a>
                            <a href="#" class="button button-border button-rounded button-aqua"><i class="icon-printer"></i>Yazdır</a>

                            <div class="clear"></div>
                            <div class="line"></div>

                            <!-- Product Single - Short Description
                            ============================================= -->
                            <p>Güneydoğu Asya Çinhindi Yarımadası’nın doğusunda bulunan Vietnam’ın arkeolojik tarihi, 2500 yıl öncesine dayanmaktadır.  Dünya ülkeleri sıralamasında 70.sırada olan Kamboçya ise tarihsel varlığının izleri M.Ö. 5.yy’a dayanan ve aslında çok da iç açıcı olmayan bir geçmişe sahiptir. </p>
                            <p>
                            Sizi burada ilk karşılayacak doğanın muhteşem atmosferi... Halong Körfezi’nde gün batımı, kireçtaşı adalarının gerçek üstü deniz manzarası, yüzyılın etkilerini taşıyan görkemli Angkor Wat tapınağı ve gezerken adeta tarihin derinliklerine ineceğiniz Savaş Müzesi...Gelin, bu büyüleyici yolculuğun keyfini birlikte çıkaralım</p>
                            <ul class="iconlist">
                                <li><i class="icon-caret-right"></i> Kuzey Vietnamlı Viet Kong savaşçıları tarafından yer altına yapılmış 200 km uzunluğundaki
 Cu Chi Tünelleri </li>
                                <li><i class="icon-caret-right"></i> Mekong Deltası Turu</li>
                                <li><i class="icon-caret-right"></i> Notre Dame Katedrali</li>
                                <li><i class="icon-caret-right"></i> Kamboçya’nın kuzey batısındaki Siem  Reap şehri turu</li>
                                <li><i class="icon-caret-right"></i> Ünlü tapınak Angkor Wat</li>
                                <li><i class="icon-caret-right"></i> Aspara Dans Gösterizi</li>
                            </ul><!-- Product Single - Short Description End -->

                            <!-- Product Single - Meta
                            ============================================= -->
                            <div class="panel panel-default product-meta">
                                <div class="panel-body">
                                    <span itemprop="productID" class="sku_wrapper">Tur Kodu: <span class="sku">{{ $tour->id }}</span></span>
                                    <span class="posted_in">Kategori: <a href="#" rel="tag">Kültür Turu</a>.</span>
                                    <span class="tagged_as">Etiket: <a href="#" rel="tag">Seyahat</a>, <a href="#" rel="tag">Gezi </a>, <a href="#" rel="tag">Uzak Doğu</a>, <a href="#" rel="tag">Vietnam</a>, <a href="#" rel="tag">Kamboçya </a>, <a href="#" rel="tag">Laos</a>, <a href="#" rel="tag">Doğa</a>, <a href="#" rel="tag">Laos</a>, <a href="#" rel="tag">Halong</a>, <a href="#" rel="tag">Tatil</a>.</span>
                                </div>
                            </div><!-- Product Single - Meta End -->

                            <!-- Product Single - Share
                            ============================================= -->
                            <div class="si-share noborder clearfix">
                                <span>Paylaş:</span>
                                <div>
                                    <a href="#" class="social-icon si-borderless si-facebook">
                                        <i class="icon-facebook"></i>
                                        <i class="icon-facebook"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-twitter">
                                        <i class="icon-twitter"></i>
                                        <i class="icon-twitter"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-pinterest">
                                        <i class="icon-pinterest"></i>
                                        <i class="icon-pinterest"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-gplus">
                                        <i class="icon-gplus"></i>
                                        <i class="icon-gplus"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-rss">
                                        <i class="icon-rss"></i>
                                        <i class="icon-rss"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-email3">
                                        <i class="icon-email3"></i>
                                        <i class="icon-email3"></i>
                                    </a>
                                </div>
                            </div><!-- Product Single - Share End -->

                        </div>

                        <div class="col_one_fifth col_last">

                            <a href="#" title="Brand Logo" class="hidden-xs"><img src="http://www.siamtur.com/image/havayollari/<?php echo $havayolu; ?>.jpg" style="width:116px; height:104px;" /></a>

                            <div class="divider divider-center"><i class="icon-circle-blank"></i></div>

                            <div class="feature-box fbox-plain fbox-dark fbox-small">
                                <div class="fbox-icon">
                                    <i class="icon-thumbs-up2"></i>
                                </div>
                                <h3>Güzergah</h3>
                                <p class="notopmargin">{{ $tour->yol }}</p>
                            </div>

                            <!--<div class="feature-box fbox-plain fbox-dark fbox-small">
                                <div class="fbox-icon">
                                    <i class="icon-credit-cards"></i>
                                </div>
                                <h3>Vize Yok!</h3>
                                <p class="notopmargin">We accept Visa, MasterCard and American Express.</p>
                            </div>-->

                            <div class="feature-box fbox-plain fbox-dark fbox-small">
                                <div class="fbox-icon">
                                    <i class="icon-truck2"></i>
                                </div>
                                <h3>Erken Rezervasyon!</h3>
                                <!--<p class="notopmargin">Free Delivery to 100+ Locations on orders above $40.</p>-->
                            </div>

                            <!--<div class="feature-box fbox-plain fbox-dark fbox-small">
                                <div class="fbox-icon">
                                    <i class="icon-undo"></i>
                                </div>
                                <h3>30-Days Returns</h3>
                                <p class="notopmargin">Return or exchange items purchased within 30 days.</p>
                            </div>-->

                        </div>

                        <div class="col_full nobottommargin">

                            <div class="col-md-8 gezi-programi-icerik">                            
                                <?=str_replace('src="../', 'src="http://siamtur.com/', $geziProgrami->icerik)?>
                                
                            </div>

                            <div class="col-md-4">

                                <div class="olsun olsun2 polsun" id="tab-3" style="font-family: Arial !important;">
                                    <?php
                                    if($tour->turfiyat=="3")
                                    {
                                        if($turkod == "35")
                                        {
                                    ?>
                                    <div class="" style="margin-top:10px; margin-bottom:30px;">
                                    Maldivler'de direkt kontratlı otellerimiz ve anlaşmalı uçak fiyatlarımızla en uygun fiyatları sizlerin ihtiyaçlarına göre hazırlayabilmemiz için bize <a href="#" data-toggle="modal" data-target="#goruntule_modal">buradan</a> ulaşarak teklif alabilirsiniz.
                                    </div>                                    
                                    <?php
                                        }
                                        else
                                        {
                                    ?>
                                    <table id="goruntule_tablo" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th class="olmasin-fiyat">Kategori</th>
                                        <th>Otel</th>
                                        <th>Oda</th>
                                        <th class="olmasin-fiyat">Yemek</th>
                                        <th class="olmasin-fiyat">Transfer</th>
                                        <th>Tarih Aralığı </th>
                                        <th>Kişi Başı Tur Fiyatı</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th class="olmasin-fiyat">Kategori</th>
                                        <th>Otel</th>
                                        <th>Oda</th>
                                        <th class="olmasin-fiyat">Yemek</th>
                                        <th class="olmasin-fiyat">Transfer</th>
                                        <th>Tarih Aralığı</th>
                                        <th>Kişi Başı Tur Fiyatı</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php                                    
                                    foreach ($fiyat as $sorgu) {
                                        $giristarih=ZamanDuzenle2($sorgu->giristarih);
                                        echo"<tr>
                                            <td class=\"olmasin-fiyat\">$sorgu->kategori</td>
                                            <td>$sorgu->otel</td>
                                            <td>$sorgu->oda</td>
                                            <td class=\"olmasin-fiyat\">$sorgu->yemek</td>
                                            <td class=\"olmasin-fiyat\">$sorgu->transfer</td>
                                            <td><span class=\"yokol\">$sorgu->giristarih</span>$sorgu->tariharaligi</td>
                                            <td>$sorgu->fiyat $</td>
                                        </tr>";
                                    }                                    
                                        ?>

                                    </tbody>
                                    </table>
                                    <?php
                                        }
                                    }
                                    else {
                                        ?>
                                        <?php
                                       
                                        
                                        $genel_fiyat = $fiyatlar->icerik;
                                        if ($tour->satin_al == "1") {
                                            foreach ($price as $sorgu_price) {
                                            echo
                                            "
                                            <div class=\"row padding-temizle\">
                                            <div class=\"\"><b>Paket</b></div>
                                            <div class=\"\">$sorgu_price->paket</div>
                                            </div>
                                            <hr>
                                            <div class=\"row padding-temizle\">
                                            <div class=\"\"><b>İki Kişilik Oda Kişi Başı Fiyat</b></div>
                                            <div class=\"\">$sorgu_price->ikikisilik</div>
                                            </div>
                                            <hr>
                                            <div class=\"row padding-temizle\">
                                            <div class=\"\"><b>3.Kişi/Ek Yatak</b></div>
                                            <div class=\"\">$sorgu_price->ekyatak</div>
                                            </div>
                                            <hr>
                                            <div class=\"row padding-temizle\">
                                            <div class=\"\"><b>Tek Kişilik Oda Farkı</b></div>
                                            <div class=\"\">$sorgu_price->tekkisilik</div>
                                            </div>
                                            <hr>
                                            <div class=\"row padding-temizle\">
                                            <div class=\"\"><b>2-12 Yaş Çocuk Fiyatı</b></div>
                                            <div class=\"\">$sorgu_price->cocukfiyat</div>
                                            </div>
                                            ";
                                        }
                                            /*echo "<br>
                        <table style=\"width:100%; margin-top:50px;\">
                        <tr style=\"text-align:center; font-weight:bold;color:#000;\" class=\"fiyat-mo\"><td>Paket</td><td>İki Kişilik <br> Odada Kişi <br> Başı Tur Fiyat</td><td>3.Kişi/Ek <br> Yatak</td><td>Tek Kişilik <br> Oda Farkı</td><td>2-12 Yaş <br> Çocuk Fiyatı</td></tr>
                        <tr style=\"height:5px;\"></tr>
                        ";


                        foreach ($price as $sorgu_price) {

                            echo "<tr class=\"fiyat-mo\" style=\"text-align:center;\"><td>$sorgu_price->paket</td><td>$sorgu_price->ikikisilik</td><td>$sorgu_price->ekyatak</td><td>$sorgu_price->tekkisilik</td><td>$sorgu_price->cocukfiyat</td></tr><tr style=\"height:5px;\"></tr>";
                        }
                                            
                                            echo "
                        <tr style=\"height:5px;\"></tr>
                        <tr style=\"height:5px;\"></tr>
                        <tr><td colspan=\"5\">$fiyatlar->detay</td></tr>
                        </table>
                ";*/
                                        } else {
                                            echo "<br><table width=\"100%\"><tr height=\"20\"></tr><tr><td width=\"100%\">$genel_fiyat</td></tr></table>";
                                        }
                                    }

                                    ?>
                                </div>
                                <div class="row genel-sartlar-edit">

                                    {!! $genelSartlar->icerik !!}

                                </div>
                                <div class="row">



                                </div>

                            </div>


                        </div>

                    </div>

                </div>

                <div class="clear"></div><div class="line"></div>

                <!--<div class="col_full nobottommargin">

                    <h4>Benzer Turlar</h4>

                    <div id="oc-product" class="owl-carousel product-carousel carousel-widget" data-margin="30" data-pagi="false" data-autoplay="5000" data-items-xxs="1" data-items-sm="2" data-items-md="3" data-items-lg="4">

                        <div class="oc-item">
                            <div class="product iproduct clearfix">
                                <div class="product-image">
                                    <a href="#"><img src="/images/shop/dress/1.jpg" alt="Checked Short Dress"></a>
                                    <a href="#"><img src="/images/shop/dress/1-1.jpg" alt="Checked Short Dress"></a>
                                    <div class="sale-flash">50% Off*</div>
                                    <div class="product-overlay">
                                        <a href="#" class="add-to-cart"><i class="icon-shopping-cart"></i><span> Add to Cart</span></a>
                                        <a href="/include/ajax/shop-item.html" class="item-quick-view" data-lightbox="ajax"><i class="icon-zoom-in2"></i><span> Quick View</span></a>
                                    </div>
                                </div>
                                <div class="product-desc center">
                                    <div class="product-title"><h3><a href="#">Checked Short Dress</a></h3></div>
                                    <div class="product-price"><del>$24.99</del> <ins>$12.49</ins></div>
                                    <div class="product-rating">
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star-half-full"></i>
                                    </div>
                                </div>
                            </div>
                        </div>                       

                    </div>

                </div>-->

            </div>

        </div>

    </section><!-- #content end -->
@endsection