@extends('layouts.template0.master')
@section('header-sector')
<title>{{$seo->sayfa_ad}}</title>
@endsection
@section('content')

    <!-- Page Title
		============================================= -->
    <section id="page-title">

        <div class="container clearfix">
            <h1>{{ $tour->tur_ad_gor }}</h1>
            <ol class="breadcrumb">
                <li><a href="#">Anasayfa</a></li>
                <li><a href="#">Turlarımız</a></li>
                <li class="active">{{ $tour->tur_ad_gor }}</li>
            </ol>
        </div>

    </section><!-- #page-title end -->

    <!-- Content
    ============================================= -->
    <section id="content">

        <div class="content-wrap">

            <div class="container clearfix">

                <div class="single-product">

                    <div class="product">

                        <div class="col_two_fifth">

                            <!-- Product Single - Gallery
                            ============================================= -->
                            <div class="product-image">
                                <div class="fslider" data-pagi="false" data-arrows="false" data-thumbs="true">
                                    <div class="flexslider">
                                        <div class="slider-wrap" data-lightbox="gallery">
                                            @foreach($fotoGaleri as $foto)
                                            <div class="slide" data-thumb="http://siamtur.com/s1/{{ $foto->resim }}"><a href="http://siamtur.com/s1/{{ $foto->resim }}" title="{{ $tour->tur_ad_gor }}" data-lightbox="gallery-item"><img src="http://siamtur.com/s1/{{ $foto->resim }}" alt="{{ $tour->tur_ad_gor }}"></a></div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="sale-flash">Son 2 Kişi!</div>
                            </div><!-- Product Single - Gallery End -->

                        </div>

                        <div class="col_two_fifth product-desc">

                            <!-- Product Single - Price
                            ============================================= -->
                            <div class="product-price"><del><?php $del= $tour->ana_fiyat+200; echo $del; ?> $</del> <ins>{{ $tour->ana_fiyat }} $</ins></div><!-- Product Single - Price End -->

                            <!-- Product Single - Rating
                            ============================================= -->
                            <div class="product-rating">
                                <i class="icon-star3"></i>
                                <i class="icon-star3"></i>
                                <i class="icon-star3"></i>
                                <i class="icon-star-half-full"></i>
                                <i class="icon-star-empty"></i>
                            </div><!-- Product Single - Rating End -->

                            <div class="clear"></div>
                            <div class="line"></div>

                            <!-- Product Single - Price
                            ============================================= -->
                            <div class="product-price" style="color:#333; font-size:16px;">
                                <?php
                                    echo $tour->gun." Gün ".$tour->gece." Gece";
                                ?>
                            </div><!-- Product Single - Price End -->

                            <!-- Product Single - Rating
                            ============================================= -->
                            <div class="product-price" style="color:#333; float:right; font-size:16px;">
                                <?php
                                if($tour->htarihi=="0000-00-00" || $tour->htarihi==NULL)
                                {
                                    echo "HER GÜN HAREKETLİ";
                                }
                                else
                                {
                                    echo $tour->htarihi;
                                }
                                ?>
                            </div><!-- Product Single - Rating End -->

                            <div class="clear"></div>
                            <div class="line"></div>

                            <!-- Product Single - Quantity & Cart Button
                            ============================================= -->
                            <a href="#" class="button button-border button-rounded button-red"><i class="icon-mail"></i>Talep Et</a>
                            <a href="#" class="button button-border button-rounded button-aqua"><i class="icon-printer"></i>Yazdır</a>

                            <div class="clear"></div>
                            <div class="line"></div>

                            <!-- Product Single - Short Description
                            ============================================= -->
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero velit id eaque ex quae laboriosam nulla optio doloribus! Perspiciatis, libero, neque, perferendis at nisi optio dolor!</p>
                            <ul class="iconlist">
                                <li><i class="icon-caret-right"></i> Dynamic Color Options</li>
                                <li><i class="icon-caret-right"></i> Lots of Size Options</li>
                                <li><i class="icon-caret-right"></i> 30-Day Return Policy</li>
                            </ul><!-- Product Single - Short Description End -->

                            <!-- Product Single - Meta
                            ============================================= -->
                            <div class="panel panel-default product-meta">
                                <div class="panel-body">
                                    <span itemprop="productID" class="sku_wrapper">Tur Kodu: <span class="sku">{{ $tour->id }}</span></span>
                                    <span class="posted_in">Kategori: <a href="#" rel="tag">Dress</a>.</span>
                                    <span class="tagged_as">Etiket: <a href="#" rel="tag">Pink</a>, <a href="#" rel="tag">Short</a>, <a href="#" rel="tag">Dress</a>, <a href="#" rel="tag">Printed</a>.</span>
                                </div>
                            </div><!-- Product Single - Meta End -->

                            <!-- Product Single - Share
                            ============================================= -->
                            <div class="si-share noborder clearfix">
                                <span>Paylaş:</span>
                                <div>
                                    <a href="#" class="social-icon si-borderless si-facebook">
                                        <i class="icon-facebook"></i>
                                        <i class="icon-facebook"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-twitter">
                                        <i class="icon-twitter"></i>
                                        <i class="icon-twitter"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-pinterest">
                                        <i class="icon-pinterest"></i>
                                        <i class="icon-pinterest"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-gplus">
                                        <i class="icon-gplus"></i>
                                        <i class="icon-gplus"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-rss">
                                        <i class="icon-rss"></i>
                                        <i class="icon-rss"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-email3">
                                        <i class="icon-email3"></i>
                                        <i class="icon-email3"></i>
                                    </a>
                                </div>
                            </div><!-- Product Single - Share End -->

                        </div>

                        <div class="col_one_fifth col_last">

                            <a href="#" title="Brand Logo" class="hidden-xs"><img src="http://www.siamtur.com/image/havayollari/<?php echo $havayolu; ?>.jpg" style="width:116px; height:104px;" /></a>

                            <div class="divider divider-center"><i class="icon-circle-blank"></i></div>

                            <div class="feature-box fbox-plain fbox-dark fbox-small">
                                <div class="fbox-icon">
                                    <i class="icon-thumbs-up2"></i>
                                </div>
                                <h3>Güzergah</h3>
                                <p class="notopmargin">{{ $tour->yol }}</p>
                            </div>

                            <div class="feature-box fbox-plain fbox-dark fbox-small">
                                <div class="fbox-icon">
                                    <i class="icon-credit-cards"></i>
                                </div>
                                <h3>Vize Yok!</h3>
                                <!--<p class="notopmargin">We accept Visa, MasterCard and American Express.</p>-->
                            </div>

                            <div class="feature-box fbox-plain fbox-dark fbox-small">
                                <div class="fbox-icon">
                                    <i class="icon-truck2"></i>
                                </div>
                                <h3>Erken Rezervasyon!</h3>
                                <!--<p class="notopmargin">Free Delivery to 100+ Locations on orders above $40.</p>-->
                            </div>

                            <div class="feature-box fbox-plain fbox-dark fbox-small">
                                <div class="fbox-icon">
                                    <i class="icon-undo"></i>
                                </div>
                                <h3>30-Days Returns</h3>
                                <p class="notopmargin">Return or exchange items purchased within 30 days.</p>
                            </div>

                        </div>

                        <div class="col_full nobottommargin">

                            <div class="tabs clearfix nobottommargin" id="tab-1">

                                <ul class="tab-nav clearfix">
                                    <li><a href="#tabs-1"><i class="icon-align-justify2"></i><span class="hidden-xs"> Gezi Programı</span></a></li>
                                    <li><a href="#tabs-2"><i class="icon-info-sign"></i><span class="hidden-xs"> Genel Şartlar</span></a></li>
                                    <li><a href="#tabs-3"><i class="icon-star3"></i><span class="hidden-xs"> Fiyatlar</span></a></li>
                                    <li><a href="#tabs-4"><i class="icon-star3"></i><span class="hidden-xs"> Ekstra Turlar</span></a></li>
                                </ul>

                                <div class="tab-container">

                                    <div class="tab-content clearfix" id="tabs-1">
                                        {!! $geziProgrami->icerik !!}
                                    </div>
                                    <div class="tab-content clearfix" id="tabs-2">

                                        {!! $genelSartlar->icerik !!}

                                    </div>
                                    <div class="tab-content clearfix" id="tabs-3">



                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="clear"></div><div class="line"></div>

                <div class="col_full nobottommargin">

                    <h4>Benzer Turlar</h4>

                    <div id="oc-product" class="owl-carousel product-carousel carousel-widget" data-margin="30" data-pagi="false" data-autoplay="5000" data-items-xxs="1" data-items-sm="2" data-items-md="3" data-items-lg="4">

                        <div class="oc-item">
                            <div class="product iproduct clearfix">
                                <div class="product-image">
                                    <a href="#"><img src="/images/shop/dress/1.jpg" alt="Checked Short Dress"></a>
                                    <a href="#"><img src="/images/shop/dress/1-1.jpg" alt="Checked Short Dress"></a>
                                    <div class="sale-flash">50% Off*</div>
                                    <div class="product-overlay">
                                        <a href="#" class="add-to-cart"><i class="icon-shopping-cart"></i><span> Add to Cart</span></a>
                                        <a href="/include/ajax/shop-item.html" class="item-quick-view" data-lightbox="ajax"><i class="icon-zoom-in2"></i><span> Quick View</span></a>
                                    </div>
                                </div>
                                <div class="product-desc center">
                                    <div class="product-title"><h3><a href="#">Checked Short Dress</a></h3></div>
                                    <div class="product-price"><del>$24.99</del> <ins>$12.49</ins></div>
                                    <div class="product-rating">
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star-half-full"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="oc-item">
                            <div class="product iproduct clearfix">
                                <div class="product-image">
                                    <a href="#"><img src="/images/shop/pants/1-1.jpg" alt="Slim Fit Chinos"></a>
                                    <a href="#"><img src="/images/shop/pants/1.jpg" alt="Slim Fit Chinos"></a>
                                    <div class="product-overlay">
                                        <a href="#" class="add-to-cart"><i class="icon-shopping-cart"></i><span> Add to Cart</span></a>
                                        <a href="include/ajax/shop-item.html" class="item-quick-view" data-lightbox="ajax"><i class="icon-zoom-in2"></i><span> Quick View</span></a>
                                    </div>
                                </div>
                                <div class="product-desc center">
                                    <div class="product-title"><h3><a href="#">Slim Fit Chinos</a></h3></div>
                                    <div class="product-price">$39.99</div>
                                    <div class="product-rating">
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star-half-full"></i>
                                        <i class="icon-star-empty"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="oc-item">
                            <div class="product iproduct clearfix">
                                <div class="product-image">
                                    <a href="#"><img src="/images/shop/shoes/1-1.jpg" alt="Dark Brown Boots"></a>
                                    <a href="#"><img src="/images/shop/shoes/1.jpg" alt="Dark Brown Boots"></a>
                                    <div class="product-overlay">
                                        <a href="#" class="add-to-cart"><i class="icon-shopping-cart"></i><span> Add to Cart</span></a>
                                        <a href="include/ajax/shop-item.html" class="item-quick-view" data-lightbox="ajax"><i class="icon-zoom-in2"></i><span> Quick View</span></a>
                                    </div>
                                </div>
                                <div class="product-desc center">
                                    <div class="product-title"><h3><a href="#">Dark Brown Boots</a></h3></div>
                                    <div class="product-price">$49</div>
                                    <div class="product-rating">
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star-empty"></i>
                                        <i class="icon-star-empty"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="oc-item">
                            <div class="product iproduct clearfix">
                                <div class="product-image">
                                    <a href="#"><img src="/images/shop/dress/2.jpg" alt="Light Blue Denim Dress"></a>
                                    <a href="#"><img src="/images/shop/dress/2-2.jpg" alt="Light Blue Denim Dress"></a>
                                    <div class="product-overlay">
                                        <a href="#" class="add-to-cart"><i class="icon-shopping-cart"></i><span> Add to Cart</span></a>
                                        <a href="include/ajax/shop-item.html" class="item-quick-view" data-lightbox="ajax"><i class="icon-zoom-in2"></i><span> Quick View</span></a>
                                    </div>
                                </div>
                                <div class="product-desc center">
                                    <div class="product-title"><h3><a href="#">Light Blue Denim Dress</a></h3></div>
                                    <div class="product-price">$19.95</div>
                                    <div class="product-rating">
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star-empty"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="oc-item">
                            <div class="product iproduct clearfix">
                                <div class="product-image">
                                    <a href="#"><img src="/images/shop/sunglasses/1.jpg" alt="Unisex Sunglasses"></a>
                                    <a href="#"><img src="/images/shop/sunglasses/1-1.jpg" alt="Unisex Sunglasses"></a>
                                    <div class="sale-flash">Sale!</div>
                                    <div class="product-overlay">
                                        <a href="#" class="add-to-cart"><i class="icon-shopping-cart"></i><span> Add to Cart</span></a>
                                        <a href="include/ajax/shop-item.html" class="item-quick-view" data-lightbox="ajax"><i class="icon-zoom-in2"></i><span> Quick View</span></a>
                                    </div>
                                </div>
                                <div class="product-desc center">
                                    <div class="product-title"><h3><a href="#">Unisex Sunglasses</a></h3></div>
                                    <div class="product-price"><del>$19.99</del> <ins>$11.99</ins></div>
                                    <div class="product-rating">
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star-empty"></i>
                                        <i class="icon-star-empty"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section><!-- #content end -->
@endsection