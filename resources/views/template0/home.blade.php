@extends('layouts.template0.master')
@section('content')

<!--<section id="slider" class="slider-parallax" style="background-color: #222;">

    <div id="oc-slider" class="owl-carousel carousel-widget" data-margin="0" data-items="1" data-pagi="false" data-loop="true" data-animate-in="rollIn" data-speed="450" data-animate-out="rollOut" data-autoplay="5000">

        <a href="#"><img src="http://siamtur.com/s1/slider/1482419899_my_slider.jpg" alt="Slider"></a>
        <a href="#"><img src="http://siamtur.com/s1/slider/1497449613_phuket_slider.jpg" alt="Slider"></a>
        <a href="#"><img src="http://siamtur.com/s1/slider/1482487533_ghs_slider_2.jpg" alt="Slider"></a>
        <a href="#"><img src="http://siamtur.com/s1/slider/1501764549_banner3.jpg" alt="Slider"></a>

    </div>

</section>-->

<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">

            <!-- Post Content
            ============================================= -->
            <div class="postcontent nobottommargin col_last">

                <!-- Shop
                ============================================= -->
                <div id="shop" class="shop product-3 grid-container clearfix">
                    @foreach($data2Tours as $data2Tour)
                    @foreach($data2Tour as $value)
                    <?php
                    $slug = str_slug($value->seo_link); 
                    ?>
                    <div class="product sf-{{$value->slug}} clearfix">
                        <div class="product-image">
                            <a href="/{{$slug}}/{{$value->id}}"><img src="http://siamtur.com/s1/{{ $value->resim }}" alt="{{ $value->tur_ad }}" style="height: 160px;"></a>
                            <a href="/{{$slug}}/{{$value->id}}"><img src="http://siamtur.com/s1/{{ $value->resim }}" alt="{{ $value->tur_ad }}" style="height: 160px;"></a>
                            <div class="sale-flash">Erken Rezervasyon*</div>                            
                        </div>
                        <div class="product-desc center">
                            <div class="product-title"><h3><a href="/{{$value->slug}}/{{$value->id}}">{{ $value->tur_ad }}</a></h3></div>
                            <div class="product-title"><h3>{{ Helper::DateConvertTurkishDMY("$value->htarihi") }}</h3></div>
                            <div class="product-price"><del><?= $value->ana_fiyat + 150; ?> $</del> <ins>{{ $value->ana_fiyat }} $</ins></div>
                            <div class="product-rating">
                                <i class="fa fa-credit-card"></i>
                                <i class="fa fa-cc-visa"></i>
                                <i class="fa-plane"></i>
                                <i class="fa-car"></i>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endforeach

                    

                </div><!-- #shop end -->

            </div><!-- .postcontent end -->

            <!-- Sidebar
            ============================================= -->
            <div class="sidebar nobottommargin">
                <div class="sidebar-widgets-wrap">

                    <div class="widget widget-filter-links clearfix">

                        <h4>Filtre</h4>
                        <ul class="custom-filter" data-container="#shop" data-active-class="active-filter">
                            <li class="widget-filter-reset active-filter"><a href="#" data-filter="*">Temizle</a></li>
                            @foreach($filtre as $val)
                            <li><a href="#" data-filter=".sf-{{$val->slug}}">{{$val->title}}</a></li>
                            @endforeach
                            <!--<li><a href="#" data-filter=".sf-tshirt">Kamboçya Turları</a></li>
                            <li><a href="#" data-filter=".sf-pant">Laos Turları</a></li>
                            <li><a href="#" data-filter=".sf-sunglass">Kurban Bayramı Turları</a></li>
                            <li><a href="#" data-filter=".sf-shoes">Ramazan Bayramı Turları</a></li>
                            <li><a href="#" data-filter=".sf-watch">Sömestre Turları</a></li>
                            <li><a href="#" data-filter=".sf-watch">THY ile Vietnam Turları</a></li>
                            <li><a href="#" data-filter=".sf-watch">THY ile Kamboçya Turları</a></li>
                            <li><a href="#" data-filter=".sf-watch">Yılbaşı Turları</a></li>
                            <li><a href="#" data-filter=".sf-watch">Vietnam Kurban Bayramı Turları</a></li>
                            <li><a href="#" data-filter=".sf-watch">Kamboçya Kurban Turları</a></li>
                            <li><a href="#" data-filter=".sf-watch">Erken Rezervasyon Turları</a></li>
                            <li><a href="#" data-filter=".sf-watch">Özel Tarih Turları</a></li>-->
                        </ul>

                    </div>

                    <!--<div class="widget widget-filter-links clearfix">

                        <h4>Sırala</h4>
                        <ul class="shop-sorting">
                            <li class="widget-filter-reset active-filter"><a href="#" data-sort-by="original-order">Temizle</a></li>
                            <li><a href="#" data-sort-by="name">İsme Göre</a></li>
                            <li><a href="#" data-sort-by="price_lh">Fiyat: Düşükten Yükseğe</a></li>
                            <li><a href="#" data-sort-by="price_hl">Fiyat: Yüksekten Düşüğe</a></li>
                        </ul>

                    </div>-->

                </div>
            </div><!-- .sidebar end -->

        </div>

    </div>

    <div class="content-wrap">

        <div class="section bottommargin-lg header-stick slogan-1">
            <div class="container clear-bottommargin clearfix">

                <div class="row topmargin-sm bottommargin-sm">

                    <div class="col-md-3 col-sm-6 bottommargin">
                        <i class="i-plain color i-large icon-line2-plane inline-block" style="margin-bottom: 15px;"></i>
                        <div class="heading-block nobottomborder nobottommargin">
                            <span class="before-heading">Kolay &amp; Ucuz.</span>
                            <h4>Vietnam, Kamboçya ve Laos da En Uygun Fiyatlı Uçuşlar</h4>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 bottommargin">
                        <i class="i-plain color i-large icon-line2-key inline-block" style="margin-bottom: 15px;"></i>
                        <div class="heading-block nobottomborder nobottommargin">
                            <span class="before-heading">100 den Fazla Otelle Anlaşmamız var.</span>
                            <h4>En Uygun Otel Ücretleri</h4>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 bottommargin">
                        <i class="i-plain color i-large icon-line2-present inline-block" style="margin-bottom: 15px;"></i>
                        <div class="heading-block nobottomborder nobottommargin">
                            <span class="before-heading">Özel Hareketli Grup Turlarımız.</span>
                            <h4>Özel Hareketli Turlarımızı Kaçırmayın</h4>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 bottommargin">
                        <i class="i-plain color i-large icon-line2-earphones-alt inline-block" style="margin-bottom: 15px;"></i>
                        <div class="heading-block nobottomborder nobottommargin">
                            <span class="before-heading">Bize Ulaşın.</span>
                            <h4>444 3 695</h4>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <div class="container clearfix">

            <div class="heading-block center nobottomborder">
                <span class="before-heading color">Aklınıza bir şey mi takıldı?</span>
                <h2>Vietnam ve Kamboçya da En Çok Sorulanlar</h2>
            </div>

        </div>

        <div id="portfolio" class="portfolio grid-container portfolio-nomargin portfolio-full portfolio-overlay-open clearfix">
            @foreach($showcases as $showcase)
            <article class="portfolio-item pf-media pf-icons">
                <div class="portfolio-image">
                    <a href="/{{ $showcase->slug }}">
                        <img src="http://s1crm.siamtur.com{{ $showcase->page_img }}" alt="{{ $showcase->title }}">
                        <div class="portfolio-overlay">
                            <div class="portfolio-desc">
                                <h3 style="font-family: 'Raleway', sans-serif !important;">{{ $showcase->title }}</h3>
                            </div>
                        </div>
                    </a>
                </div>
            </article>
            @endforeach

            <!--<article class="portfolio-item pf-illustrations">
                <div class="portfolio-image">
                    <a href="portfolio-single.html">
                        <img src="/demos/travel/images/packages/2.jpg" alt="Romantic Getaways">
                        <div class="portfolio-overlay">
                            <div class="portfolio-desc">
                                <h3>ROMANTİZM</h3>
                            </div>
                        </div>
                    </a>
                </div>
            </article>

            <article class="portfolio-item pf-graphics pf-uielements">
                <div class="portfolio-image">
                    <a href="#">
                        <img src="/demos/travel/images/packages/3.jpg" alt="Mountain Madness">
                        <div class="portfolio-overlay">
                            <div class="portfolio-desc">
                                <h3>DAĞCILIK</h3>
                            </div>
                        </div>
                    </a>
                </div>
            </article>

            <article class="portfolio-item pf-icons pf-illustrations">
                <div class="portfolio-image">
                    <a href="portfolio-single.html">
                        <img src="/demos/travel/images/packages/4.jpg" alt="Active Explorer">
                        <div class="portfolio-overlay">
                            <div class="portfolio-desc">
                                <h3>KAŞİF</h3>
                            </div>
                        </div>
                    </a>
                </div>
            </article>

            <article class="portfolio-item pf-uielements pf-media">
                <div class="portfolio-image">
                    <a href="portfolio-single.html">
                        <img src="/demos/travel/images/packages/5.jpg" alt="Icy Challenge">
                        <div class="portfolio-overlay">
                            <div class="portfolio-desc">
                                <h3>KAR SPORLARI</h3>
                            </div>
                        </div>
                    </a>
                </div>
            </article>

            <article class="portfolio-item pf-graphics pf-illustrations">
                <div class="portfolio-image">
                    <a href="portfolio-single.html">
                        <img src="/demos/travel/images/packages/6.jpg" alt="Hill Trekking">
                        <div class="portfolio-overlay" data-lightbox="gallery">
                            <div class="portfolio-desc">
                                <h3>TREKKİNG</h3>
                            </div>
                        </div>
                    </a>
                </div>
            </article>

            <article class="portfolio-item pf-uielements pf-icons">
                <div class="portfolio-image">
                    <a href="portfolio-single-video.html">
                        <img src="/demos/travel/images/packages/7.jpg" alt="Forest Camping">
                        <div class="portfolio-overlay">
                            <div class="portfolio-desc">
                                <h3>KAMPÇILIK</h3>
                            </div>
                        </div>
                    </a>
                </div>
            </article>

            <article class="portfolio-item pf-graphics">
                <div class="portfolio-image">
                    <a href="portfolio-single.html">
                        <img src="/demos/travel/images/packages/8.jpg" alt="River Rafting">
                        <div class="portfolio-overlay">
                            <div class="portfolio-desc">
                                <h3>RAFTİNG</h3>
                            </div>
                        </div>
                    </a>
                </div>
            </article>-->

        </div>

<!--<img src="http://siamtur.com/s1/{{ $value->resim }}"-->
        <div class="common-height clearfix">

            <div id="popular-dest-map" class="col-md-8 col-padding gmap hidden-xs"></div>

            <div class="col-md-4 col-padding" style="background-color: #F9F9F9;">
                <div class="max-height clearfix">
                    <div class="heading-block nobottommargin">
                        <h4>POPÜLER TURLARIMIZ</h4>
                    </div>
                    @foreach($data3Tours as $allTour)
                    <?php
                    $slug = str_slug($allTour->seo_link); 
                    ?>
                        <div class="spost col-md-12 col-sm-6 noborder noleftpadding clearfix">
                            <div class="entry-image">
                                <a href="/{{$slug}}/{{$allTour->id}}"><img src="http://siamtur.com/s1/{{ $allTour->resim }}" alt=""></a>
                            </div>
                            <div class="entry-c">
                                <div class="entry-title">
                                    <h4><a href="/{{$slug}}/{{$allTour->id}}">{{ $allTour->tur_ad_gor }}</a></h4>
                                </div>
                                <ul class="entry-meta">
                                    <li><span class="color">{{ $allTour->gun }} gün {{ $allTour->gece }} gece / {{ $allTour->ana_fiyat }} $</span></li>
                                </ul>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>


        </div>

        <!--<a href="#" class="button button-full button-dark center bottommargin-lg">
            <div class="container clearfix">
                İstediğiniz turu bulamadınız mı? <strong>Tüm Turlar</strong> <i class="icon-caret-right" style="top:4px;"></i>
            </div>
        </a>-->

        <div class="container clear-bottommargin clearfix">

            <div class="row clearfix">

                <div class="heading-block center">
                    <h2>BLOG</h2>
                    <span>Son yazılarımıza buradan ulaşabilirsiniz.</span>
                </div>

                <div class="ipost col-sm-6 bottommargin clearfix">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="entry-image nobottommargin">
                                <a href="http://www.siamtur.com/uzakdogugazetesi/2017/08/04/dogaustu-bir-doga-ninh-binh/"><img src="http://www.siamtur.com/uzakdogugazetesi/wp-content/uploads/2017/08/Ninh-Binh-2-1-630x840.jpg" style="height:225px;" alt="Paris"></a>
                            </div>
                        </div>
                        <div class="col-md-6" style="margin-top: 20px;">
                            <div class="entry-title">
                                <h3 style="font-family: 'Raleway', sans-serif !important;"><a href="#">Doğaüstü bir doğa: Ninh Binh</a></h3>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li><i class="icon-calendar3"></i> 4 Ağustos 2017</li>
                                <li><a href="#"><i class="icon-comments"></i> 19</a></li>
                            </ul>
                            <div class="entry-content">
                                <a href="http://www.siamtur.com/uzakdogugazetesi/2017/08/04/dogaustu-bir-doga-ninh-binh/" class="more-link">Okuyun</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ipost col-sm-6 bottommargin clearfix">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="entry-image nobottommargin">
                                <a href="http://www.siamtur.com/uzakdogugazetesi/2015/07/29/yer-altina-gizlenmis-umutlar-cu-chi/"><img src="http://www.siamtur.com/uzakdogugazetesi/wp-content/uploads/2015/07/Captured-Viet-Cong-soldier-630x514.jpg" style="height:225px;" alt="Paris"></a>
                            </div>
                        </div>
                        <div class="col-md-6" style="margin-top: 20px;">
                            <div class="entry-title">
                                <h3 style="font-family: 'Raleway', sans-serif !important;"><a href="#">Yer Altına Gizlenmiş Umutlar; Cu Chi</a></h3>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li><i class="icon-calendar3"></i> 29 Temmuz 2017</li>
                                <li><a href="#"><i class="icon-comments"></i> 19</a></li>
                            </ul>
                            <div class="entry-content">
                                <a href="http://www.siamtur.com/uzakdogugazetesi/2015/07/29/yer-altina-gizlenmis-umutlar-cu-chi/" class="more-link">Okuyun</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ipost col-sm-6 bottommargin clearfix">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="entry-image nobottommargin">
                                <a href="http://www.siamtur.com/uzakdogugazetesi/2015/06/15/fotograf-meraklilari-icin-uzak-doguda-7-muhtesem-nokta/"><img src="http://www.siamtur.com/uzakdogugazetesi/wp-content/uploads/2015/06/foto-630x840.jpg" style="height:225px;" alt="Paris"></a>
                            </div>
                        </div>
                        <div class="col-md-6" style="margin-top: 20px;">
                            <div class="entry-title">
                                <h3 style="font-family: 'Raleway', sans-serif !important;"><a href="#">Fotoğraf Meraklıları İçin Uzak Doğu’da 7 Muhteşem Nokta</a></h3>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li><i class="icon-calendar3"></i> 15 Haziran 2017</li>
                                <li><a href="#"><i class="icon-comments"></i> 19</a></li>
                            </ul>
                            <div class="entry-content">
                                <a href="http://www.siamtur.com/uzakdogugazetesi/2015/06/15/fotograf-meraklilari-icin-uzak-doguda-7-muhtesem-nokta/" class="more-link">Okuyun</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ipost col-sm-6 bottommargin clearfix">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="entry-image nobottommargin">
                                <a href="http://www.siamtur.com/uzakdogugazetesi/2015/06/03/45-gunde-devri-vietnam/"><img src="http://www.siamtur.com/uzakdogugazetesi/wp-content/uploads/2015/06/halong-630x840.jpg" style="height:225px;" alt="Paris"></a>
                            </div>
                        </div>
                        <div class="col-md-6" style="margin-top: 20px;">
                            <div class="entry-title">
                                <h3 style="font-family: 'Raleway', sans-serif !important;"><a href="#">45 günde devri Vietnam</a></h3>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li><i class="icon-calendar3"></i> 14 Haziran 2017</li>
                                <li><a href="#"><i class="icon-comments"></i> 19</a></li>
                            </ul>
                            <div class="entry-content">
                                <a href="http://www.siamtur.com/uzakdogugazetesi/2015/06/03/45-gunde-devri-vietnam/" class="more-link">Okuyun</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

</section><!-- #content end -->
<script>
        jQuery(document).ready( function($){
            $('#shop').isotope({
                transitionDuration: '0.65s',
                getSortData: {
                    name: '.product-title',
                    price_lh: function( itemElem ) {
                        if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
                            var price = $(itemElem).find('.product-price ins').text();
                        } else {
                            var price = $(itemElem).find('.product-price').text();
                        }

                        priceNum = price.split("$");

                        return parseFloat( priceNum[1] );
                    },
                    price_hl: function( itemElem ) {
                        if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
                            var price = $(itemElem).find('.product-price ins').text();
                        } else {
                            var price = $(itemElem).find('.product-price').text();
                        }

                        priceNum = price.split("$");

                        return parseFloat( priceNum[1] );
                    }
                },
                sortAscending: {
                    name: true,
                    price_lh: true,
                    price_hl: false
                }
            });

            $('.custom-filter:not(.no-count)').children('li:not(.widget-filter-reset)').each( function(){
                var element = $(this),
                    elementFilter = element.children('a').attr('data-filter'),
                    elementFilterContainer = element.parents('.custom-filter').attr('data-container');

                elementFilterCount = Number( jQuery(elementFilterContainer).find( elementFilter ).length );

                element.append('<span>'+ elementFilterCount +'</span>');

            });

            $('.shop-sorting li').click( function() {
                $('.shop-sorting').find('li').removeClass( 'active-filter' );
                $(this).addClass( 'active-filter' );
                var sortByValue = $(this).find('a').attr('data-sort-by');
                $('#shop').isotope({ sortBy: sortByValue });
                return false;
            });
        });
    </script>
@endsection
