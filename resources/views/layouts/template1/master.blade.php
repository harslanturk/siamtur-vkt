<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets
    ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/css/template1/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="/css/template1/style.css" type="text/css" />
    <link rel="stylesheet" href="/css/template1/swiper.css" type="text/css" />
    <link rel="stylesheet" href="/css/template1/dark.css" type="text/css" />
    <link rel="stylesheet" href="/css/template1/font-icons.css" type="text/css" />
    <link rel="stylesheet" href="/css/template1/animate.css" type="text/css" />
    <link rel="stylesheet" href="/css/template1/magnific-popup.css" type="text/css" />

    <link rel="stylesheet" href="/css/template1/responsive.css" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="/css/template1/main.css" type="text/css" />

    <!-- Document Title
    ============================================= -->
    <title>{{$allSetting->name}}</title>

</head>

<body class="stretched no-transition">

<!-- Document Wrapper
============================================= -->
<div id="wrapper" class="clearfix">

    <!-- Header
    ============================================= -->
    <header id="header" class="full-header">

        <div id="header-wrap">

            <div class="container clearfix">

                <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                <!-- Logo
                ============================================= -->
                <div id="logo">
                    <a href="/" class="standard-logo" data-dark-logo="{{$allSetting->logo}}"><img src="{{$allSetting->logo}}" alt="{{$allSetting->name}}"></a>
                    <a href="/" class="retina-logo" data-dark-logo="{{$allSetting->logo}}"><img src="{{$allSetting->logo}}" alt="{{$allSetting->name}}"></a>
                </div><!-- #logo end -->

                <!-- Primary Navigation
                ============================================= -->
                <nav id="primary-menu">

                    <ul>
                        <li class="current"><a href="/"><div>Anasayfa</div></a>
                        </li>
                        <?php foreach ($mainMenu as $key => $menu): ?>
                        <li><a href="/<?=$menu->slug?>"><div><?=$menu->title?></div></a>
                        </li>
                        <?php endforeach ?>
                        <li><a href="/iletisim"><div>İletişim</div></a>
                        </li>
                    </ul>
                </nav><!-- #primary-menu end -->

            </div>

        </div>

    </header><!-- #header end -->

    @yield('content')

    <!-- Footer
    ============================================= -->
    <footer id="footer" class="dark">

        <div class="container">

            <!-- Footer Widgets
            ============================================= -->
            <div class="footer-widgets-wrap clearfix">

                <div class="col_two_third">

                    <div class="col_one_third">

                        <div class="widget clearfix">

                            <h4>HAKKIMIZDA</h4>

                            <p style="text-align:justify !important;">
                                <strong>Varol Makine</strong> markasıyla 2014 yılından veri faaliyet gösteren firmamız sıcak kamara metal enjeksiyon makineleri revizyonu ile sektörünün öncü firmalarındandır. Kaliteli malzeme kullanımı ve özgün tasarımıyla metal enjeksiyonda yenilikçi yaklaşımlar geliştirmekte olan firmamız servis kalitesiylede hizmette devamlılık ve hızı prensip edinmiştir.
                            </p>

                        </div>

                    </div>

                    <div class="col_one_third">

                        <div class="widget widget_links clearfix">

                            <h4>Sayfalar</h4>

                            <ul>
                                <li><a href="/">Anasayfa</a></li>
                                <li><a href="/hakkimizda">Hakkımızda</a></li>
                                <li><a href="/metal-enjeksiyon">Metal Enjeksiyon</a></li>
                                <li><a href="/yedek-parca">Yedek Parça</a></li>
                                <li><a href="/servis">Servis</a></li>
                                <li><a href="/iletisim">İletişim</a></li>
                            </ul>

                        </div>

                    </div>

                    <div class="col_one_third col_last">

                        <div class="widget clearfix">
                            <h4>ÜRÜNLERİMİZ</h4>

                            <div id="post-list-footer">
                                <div class="spost clearfix">
                                    <div class="entry-c">
                                        <div class="entry-title">
                                            <h4><a href="/metal-enjeksiyon/30-ton-sicak-kamara-metal-enjeksiyon-makinesi">30 ton Sıcak Kamara Metal Enjeksiyon Makinesi</a></h4>
                                        </div>
                                    </div>
                                </div>

                                <div class="spost clearfix">
                                    <div class="entry-c">
                                        <div class="entry-title">
                                            <h4><a href="/metal-enjeksiyon/120-ton-sicak-kamara-metal-enjeksiyon-makinesi">120 ton Sıcak Kamara Metal Enjeksiyon Makinesi</a></h4>
                                        </div>
                                    </div>
                                </div>

                                <div class="spost clearfix">
                                    <div class="entry-c">
                                        <div class="entry-title">
                                            <h4><a href="/metal-enjeksiyon/200-ton-sicak-kamara-metal-enjeksiyon-makinesi">200 ton Sıcak Kamara Metal Enjeksiyon Makinesi</a></h4>
                                        </div>
                                    </div>
                                </div>

                                <div class="spost clearfix">
                                    <div class="entry-c">
                                        <div class="entry-title">
                                            <h4><a href="/metal-enjeksiyon/400-ton-sicak-kamara-metal-enjeksiyon-makinesi">400 ton Sıcak Kamara Metal Enjeksiyon Makinesi</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="col_one_third col_last">

                    <div class="widget subscribe-widget clearfix">
                        <h4>İLETİŞİM</h4>
                        <div style="background: url('http://canvashtml-cdn.semicolonweb.com/images/world-map.png') no-repeat center center; background-size: 100%;">
                            <address>
                                Üretmen İş Merkezi No: 3/26<br>
                                Topçular - Eyüp İstanbul<br>
                            </address>
                            <abbr title="Telefon"><strong>Telefon:</strong></abbr> 0(212) 567 0561<br>
                            <abbr title="Email"><strong>Email:</strong></abbr> info@varol-makine.com
                        </div>
                    </div>

                    <div class="widget clearfix" style="margin-bottom: -20px;">

                        <div class="row">

                            <img src="/img/template1/turkiyelogo.png" class="img-responsive" alt="Image" style="width:200px;">

                        </div>

                    </div>

                </div>

            </div><!-- .footer-widgets-wrap end -->

        </div>

        <!-- Copyrights
        ============================================= -->
        <div id="copyrights">

            <div class="container clearfix">

                <div class="col_half">
                    Varol Makina Copyrights &copy; 2016 Bütün Hakları Saklıdır.<br>
                    <div class="copyright-links">Bu site <a href="http://cozumlazim.com" target="_blank">Çözüm Lazım.com</a> tarafından yapılmıştır.</div>
                </div>

                <div class="col_half col_last tright">
                    <div class="fright clearfix">
                        <a href="#" class="social-icon si-small si-borderless si-facebook">
                            <i class="icon-facebook"></i>
                            <i class="icon-facebook"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-borderless si-twitter">
                            <i class="icon-twitter"></i>
                            <i class="icon-twitter"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-borderless si-linkedin">
                            <i class="icon-linkedin"></i>
                            <i class="icon-linkedin"></i>
                        </a>
                    </div>

                    <div class="clear"></div>

                    <i class="icon-envelope2"></i> info@varol-makine.com <span class="middot">&middot;</span> <i class="icon-headphones"></i> 0(212)-567-05-61 <span class="middot">&middot;</span>
                </div>

            </div>

        </div><!-- #copyrights end -->

    </footer><!-- #footer end -->

</div><!-- #wrapper end -->

<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

<!-- External JavaScripts
============================================= -->
<script type="text/javascript" src="/js/template1/jquery.js"></script>
<script type="text/javascript" src="/js/template1/plugins.js"></script>

<!-- Footer Scripts
============================================= -->
<script type="text/javascript" src="/js/template1/functions.js"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyBeSHmI1J4Cg0aGVTjD2U9-avXFaPx50oo"></script>
<script type="text/javascript" src="/js/template1/jquery.gmap.js"></script>
<script>
    jQuery(document).ready( function($){
        $('#shop').isotope({
            transitionDuration: '0.65s',
            getSortData: {
                name: '.product-title',
                price_lh: function( itemElem ) {
                    if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
                        var price = $(itemElem).find('.product-price ins').text();
                    } else {
                        var price = $(itemElem).find('.product-price').text();
                    }

                    priceNum = price.split("$");

                    return parseFloat( priceNum[1] );
                },
                price_hl: function( itemElem ) {
                    if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
                        var price = $(itemElem).find('.product-price ins').text();
                    } else {
                        var price = $(itemElem).find('.product-price').text();
                    }

                    priceNum = price.split("$");

                    return parseFloat( priceNum[1] );
                }
            },
            sortAscending: {
                name: true,
                price_lh: true,
                price_hl: false
            }
        });

        $('.custom-filter:not(.no-count)').children('li:not(.widget-filter-reset)').each( function(){
            var element = $(this),
                    elementFilter = element.children('a').attr('data-filter'),
                    elementFilterContainer = element.parents('.custom-filter').attr('data-container');

            elementFilterCount = Number( jQuery(elementFilterContainer).find( elementFilter ).length );

            element.append('<span>'+ elementFilterCount +'</span>');

        });

        $('.shop-sorting li').click( function() {
            $('.shop-sorting').find('li').removeClass( 'active-filter' );
            $(this).addClass( 'active-filter' );
            var sortByValue = $(this).find('a').attr('data-sort-by');
            $('#shop').isotope({ sortBy: sortByValue });
            return false;
        });
    });
</script>
<script type="text/javascript">

    $('#google-map').gMap({
        address: 'İstanbul, Türkiye',
        maptype: 'ROADMAP',
        zoom: 14,
        markers: [
            {
                address: "İstanbul, Türkiye",
                html: '<div style="width: 300px;"><h4 style="margin-bottom: 8px;">Hi, we\'re <span>Envato</span></h4><p class="nobottommargin">Our mission is to help people to <strong>earn</strong> and to <strong>learn</strong> online. We operate <strong>marketplaces</strong> where hundreds of thousands of people buy and sell digital goods every day, and a network of educational blogs where millions learn <strong>creative skills</strong>.</p></div>',
                icon: {
                    image: "images/icons/map-icon-red.png",
                    iconsize: [32, 39],
                    iconanchor: [32,39]
                }
            }
        ],
        doubleclickzoom: false,
        controls: {
            panControl: true,
            zoomControl: true,
            mapTypeControl: true,
            scaleControl: false,
            streetViewControl: false,
            overviewMapControl: false
        }
    });

</script>
</body>
</html>
