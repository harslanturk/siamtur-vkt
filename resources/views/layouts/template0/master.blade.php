<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />
    <title>Vietnam Kamboçya Tayland Turu</title>
    @yield('header-sector')

    <!-- Stylesheets
    ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/css/template1/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="/css/template1/style.css" type="text/css" />
    <link rel="stylesheet" href="/css/template1/dark.css" type="text/css" />

    <!-- Travel Demo Specific Stylesheet -->
    <link rel="stylesheet" href="/demos/travel/travel.css" type="text/css" />
    <link rel="stylesheet" href="/demos/travel/css/datepicker.css" type="text/css" />
    <!-- / -->

    <link rel="stylesheet" href="/css/template1/font-icons.css" type="text/css" />
    <link rel="stylesheet" href="/css/template1/animate.css" type="text/css" />
    <link rel="stylesheet" href="/css/template1/magnific-popup.css" type="text/css" />

    <link rel="stylesheet" href="/css/template1/responsive.css" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="/css/template1/colors.php?color=AC4147" type="text/css" />
    <link rel="stylesheet" type="text/css" href="/plugins/css3-rotating-words/css/style3.css" />
    <link rel="stylesheet" href="/css/template1/main.css" type="text/css" />
    <script type="text/javascript" src="/js/template1/jquery.js"></script>
    <script src="/plugins/css3-rotating-words/js/modernizr.custom.72111.js"></script>


    <!-- Document Title
    ============================================= -->
    
    <style>
@media (min-width: 992px) {

    #top-bar {
        position: absolute;
        top: 0;
        left: auto;
        right: 0;
        width: 60%;
        height: 40px;
        line-height: 39px;
        z-index: 999;
    }

    .device-md #top-bar { width: 75%; }

    .top-links ul li,
    .top-links li > a { height: 40px; }

    .top-links ul ul,
    .top-links ul div.top-link-section { top: 40px; }

    #top-social li,
    #top-social li a,
    #top-social li .ts-icon,
    #top-social li .ts-text {
        height: 40px;
        line-height: 40px;
    }


    body:not(.top-search-open) #top-bar {
        opacity: 1;
        -webkit-transition: top .4s ease, opacity .2s .2s ease;
        -o-transition: top .4s ease, opacity .2s .2s ease;
        transition: top .4s ease, opacity .2s .2s ease;
    }

    body.top-search-open #top-bar {
        opacity: 0;
        pointer-events: none;
    }


    #primary-menu:not(.style-2),
    #primary-menu:not(.style-2) > ul {
        height: 60px;
        -webkit-transition: margin .4s ease, opacity .3s ease;
        -o-transition: margin .4s ease, opacity .3s ease;
        transition: margin .4s ease, opacity .3s ease;
    }

    #primary-menu:not(.style-2) { margin-top: 40px; }

    #header.sticky-header:not(.static-sticky) #primary-menu { margin-top: 0; }

    #header #primary-menu > ul > li > a {
        padding-top: 19px;
        padding-bottom: 19px;
    }

    #primary-menu ul ul { top: 60px; }

    #top-search,
    #top-cart {
        margin-top: 20px !important;
        margin-bottom: 20px !important;
    }

    #top-cart .top-cart-content { top: 40px; }

    #top-bar-subscribe { padding: 2px 10px 4px !important; }

}

#top-bar .col_half.col_last { float: right; }

#top-social {
    display: block !important;
    float: left;
}

#top-bar-subscribe {
    float: left;
    width: 300px;
    line-height: 34px;
    padding: 5px 10px;
    border-left: 1px solid #EEE;
}

.device-sm #top-bar .col_half.col_last,
.device-xs #top-bar .col_half.col_last,
.device-xxs #top-bar .col_half.col_last { float: none; }

.device-sm #top-bar-subscribe,
.device-xs #top-bar-subscribe {
    float: right;
    border-left: 0;
}

.device-xxs #top-social,
.device-xxs #top-bar-subscribe {
    float: none;
    text-align: center;
    width: auto;
}

.device-xxs #top-social {
    border-bottom: 1px solid #EEE;
    height: 44px;
}

.device-xxs #top-social ul { display: inline-block; }

</style>

</head>

<body class="stretched">

<!-- Document Wrapper
============================================= -->
<div id="wrapper" class="clearfix">

    <!-- Top Bar
        ============================================= -->
        <div id="top-bar">

            <div class="col_half col_last clearfix nobottommargin">

                <!-- Top Social
                ============================================= -->
                <div id="top-social" class="clearfix">
                    <ul>
                        <li><a href="https://www.facebook.com/siamtur" class="si-facebook"><span class="ts-icon"><i class="icon-facebook"></i></span><span class="ts-text">Facebook</span></a></li>
                        <li><a href="https://twitter.com/siamtur" class="si-twitter"><span class="ts-icon"><i class="icon-twitter"></i></span><span class="ts-text">Twitter</span></a></li>
                        <li><a href="https://www.instagram.com/siamturizm/" class="si-instagram"><span class="ts-icon"><i class="icon-instagram"></i></span><span class="ts-text">İnstagram</span></a></li>
                        <li><a href="https://plus.google.com/u/0/+siamtur" class="si-google"><span class="ts-icon"><i class="icon-google"></i></span><span class="ts-text">Google</span></a></li>
                    </ul>
                </div><!-- #top-social end -->

                <!--<div id="top-bar-subscribe">
                    <div id="widget-subscribe-form2-result" data-notify-type="success" data-notify-msg=""></div>
                    <form id="widget-subscribe-form2" action="include/subscribe.php" role="form" method="post" class="nobottommargin">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="icon-email2"></i></span>
                            <input type="email" id="widget-subscribe-form2-email" name="widget-subscribe-form-email" class="form-control required email" placeholder="Email Giriniz">
                            <span class="input-group-btn">
                                <button class="btn btn-success" type="submit">Subscribe</button>
                            </span>
                        </div>
                    </form>
                </div>-->

            </div>

        </div><!-- #top-bar end -->

        <!-- Header
        ============================================= -->
        <header id="header" class="full-header">

            <div id="header-wrap">

                <div class="container clearfix">

                    <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                    <!-- Logo
                    ============================================= -->
                    <div id="logo">
                        <a href="/" class="standard-logo" data-dark-logo="/images/logo-dark.png">
                        <img src="/img/template1/vkt-logo.png">
                        </a>
                        <a href="/" class="retina-logo" data-dark-logo="/images/logo-dark@2x.png"><img src="/img/template1/vkt-logo.png" alt="Canvas Logo"></a>
                    </div><!-- #logo end -->

                    <!-- Primary Navigation
                    ============================================= -->
                    <nav id="primary-menu">

                        <ul>
                           <li class="current"><a href="/"><div><i class="icon-home2"></i>Anasayfa</div></a>   </li>
                            <?php foreach ($mainMenu as $key => $menu): ?>
                            @if($menu->parent==0)
                                <li><a href="/<?=$menu->slug?>"><div><i class="icon-building"></i><?=$menu->title?></div></a></li>
                            @else
                                <li><a href="#" class="sf-with-ul"><div><i class="icon-plane"></i><?=$menu->title?> <i class="icon-angle-down"></i></div></a>
                                    <ul>
                                        <?php
                                        $subMenus=App\Page::where('status','1')->where('page_cat', 'LIKE', '%"1"%')->where('parent_id', $menu->id)->orderBy('priority','asc')->get();
                                        foreach($subMenus as $subMenu){
                                        ?>
                                        <li><a href="/<?=$subMenu->slug?>"><?=$subMenu->title?></a></li>
                                        <?php
                                        }
                                        ?>
                                    </ul>
                                </li>
                            @endif
                            <?php endforeach ?>
                            <li><a href="http://www.siamtur.com/uzakdogugazetesi/" target="_blank"><div><i class="icon-building"></i>BLOG</div></a></li>                            
                        </ul>

                        <!-- Top Cart
                        ============================================= 
                        <div id="top-cart">
                            <a href="#" id="top-cart-trigger"><i class="icon-shopping-cart"></i><span>5</span></a>
                            <div class="top-cart-content">
                                <div class="top-cart-title">
                                    <h4>Favorilerim</h4>
                                </div>
                                <div class="top-cart-items">
                                    <div class="top-cart-item clearfix">
                                        <div class="top-cart-item-image">
                                            <a href="#"><img src="images/shop/small/1.jpg" alt="Blue Round-Neck Tshirt" /></a>
                                        </div>
                                        <div class="top-cart-item-desc">
                                            <a href="#">Blue Round-Neck Tshirt</a>
                                            <span class="top-cart-item-price">$19.99</span>
                                            <span class="top-cart-item-quantity">x 2</span>
                                        </div>
                                    </div>
                                    <div class="top-cart-item clearfix">
                                        <div class="top-cart-item-image">
                                            <a href="#"><img src="images/shop/small/6.jpg" alt="Light Blue Denim Dress" /></a>
                                        </div>
                                        <div class="top-cart-item-desc">
                                            <a href="#">Light Blue Denim Dress</a>
                                            <span class="top-cart-item-price">$24.99</span>
                                            <span class="top-cart-item-quantity">x 3</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="top-cart-action clearfix">
                                    <span class="fleft top-checkout-price">$114.95</span>
                                    <button class="button button-3d button-small nomargin fright">View Cart</button>
                                </div>
                            </div>
                        </div><!-- #top-cart end -->

                        <!-- Top Search
                        ============================================= 
                        <div id="top-search">
                            <a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
                            <form action="search.html" method="get">
                                <input type="text" name="q" class="form-control" value="" placeholder="Type &amp; Hit Enter..">
                            </form>
                        </div><!-- #top-search end -->

                    </nav><!-- #primary-menu end -->

                </div>

            </div>

        </header><!-- #header end -->

    @yield('content')

    <!-- Footer
    ============================================= -->
    <footer id="footer" class="dark" style="background-color: #222;">

        <div class="container">

            <!-- Footer Widgets
            ============================================= -->
            <div class="footer-widgets-wrap clearfix">

                <div class="col_one_third">

                    <div class="widget clear-bottommargin-sm clearfix">

                        <div class="row">

                            <div class="col-md-12 bottommargin-sm">
                                <div class="footer-big-contacts">
                                    <span>Bize Ulaşın:</span>
                                    444 3 695
                                </div>
                            </div>

                            <div class="col-md-12 bottommargin-sm">
                                <div class="footer-big-contacts">
                                    <span>E-Mail:</span>
                                    info@siamtur.com
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="widget subscribe-widget clearfix">
                        <div class="row">

                            <div class="col-md-6 clearfix bottommargin-sm">
                                <a href="https://www.facebook.com/siamtur" class="social-icon si-dark si-colored si-facebook nobottommargin" style="margin-right: 10px;">
                                    <i class="icon-facebook"></i>
                                    <i class="icon-facebook"></i>
                                </a>
                                <a href="https://www.facebook.com/siamtur"><small style="display: block; margin-top: 3px;"><strong>Bizi Beğenin</strong><br>Facebook</small></a>
                            </div>
                            <div class="col-md-6 clearfix">
                                <a href="https://twitter.com/siamtur" class="social-icon si-dark si-colored si-facebook nobottommargin" style="margin-right: 10px;">
                                    <i class="icon-twitter"></i>
                                    <i class="icon-twitter"></i>
                                </a>
                                <a href="https://twitter.com/siamtur"><small style="display: block; margin-top: 3px;"><strong>Takip Edin</strong><br>Twitter</small></a>
                            </div>

                        </div>
                    </div>

                </div>

                <div class="col_one_third">

                    <div class="widget clearfix">
                        <h4>Popüler Paketler</h4>

                        <div id="post-list-footer">
                            <div class="spost clearfix">
                                <div class="entry-image">
                                    <a href="http://vietnamkambocyaturu.com/aralik-vietnam-kambocya-tayland-turu/172"><img src="http://siamtur.com/s1/resimler/172/1483976957_halong-bay-vietnam_1.jpg" alt="Package"></a>
                                </div>
                                <div class="entry-c">
                                    <div class="entry-title">
                                        <h4><a href="http://vietnamkambocyaturu.com/aralik-vietnam-kambocya-tayland-turu/172">Vietnam - Kamboçya - Tayland</a></h4>
                                    </div>
                                    <ul class="entry-meta">
                                        <li><strong>2690 $</strong></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="spost clearfix">
                                <div class="entry-image">
                                    <a href="http://vietnamkambocyaturu.com/mart-vietnam-laos-kambocya-turu/174"><img src="http://siamtur.com/s1/resimler/174/1494858865_20090830_0903_luang_prabang_376.jpg" alt="Package"></a>
                                </div>
                                <div class="entry-c">
                                    <div class="entry-title">
                                        <h4><a href="http://vietnamkambocyaturu.com/mart-vietnam-laos-kambocya-turu/174">Vietnam - Laos - Kamboçya</a></h4>
                                    </div>
                                    <ul class="entry-meta">
                                        <li><strong>3190 $</strong></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="spost clearfix">
                                <div class="entry-image">
                                    <a href="http://vietnamkambocyaturu.com/nisan-vietnam-kambocya-turu/175"><img src="http://siamtur.com/s1/resimler/175/1494920653_i-VRXbk8S-L-1.jpg" alt="Package"></a>
                                </div>
                                <div class="entry-c">
                                    <div class="entry-title">
                                        <h4><a href="http://vietnamkambocyaturu.com/nisan-vietnam-kambocya-turu/175">Vietnam Kamboçya</a></h4>
                                    </div>
                                    <ul class="entry-meta">
                                        <li><strong>2490 $</strong></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col_one_third col_last">

                    <div class="widget widget_links clearfix">

                        <h4>POPÜLER DESTİNASYONLAR</h4>

                        <div class="row clearfix">
                            <div class="col-xs-6">
                                <ul>
                                    <li>Vietnam</li>
                                    <li>Kamboçya</li>
                                </ul>
                            </div>
                            <div class="col-xs-6">
                                <ul>
                                    <li>Tayland</li>
                                    <li>Laos</li>
                                </ul>
                            </div>
                        </div>

                    </div>

                    <!--<div class="widget subscribe-widget clearfix">
                        <h5>Son <strong>tekliflerimizden</strong> &amp; <strong>kampanyalarımızdan</strong> haberdar olun:</h5>
                        <div class="widget-subscribe-form-result"></div>
                        <form id="widget-subscribe-form" action="include/subscribe.php" role="form" method="post" class="nobottommargin">
                            <div class="input-group divcenter">
                                <span class="input-group-addon"><i class="icon-email2"></i></span>
                                <input type="email" id="widget-subscribe-form-email" name="widget-subscribe-form-email" class="form-control required email" placeholder="Email Giriniz">
									<span class="input-group-btn">
										<button class="btn btn-danger bgcolor" type="submit">Subscribe</button>
									</span>
                            </div>
                        </form>
                    </div>-->

                </div>

            </div><!-- .footer-widgets-wrap end -->

        </div>

        <!-- Copyrights
        ============================================= -->
        <div id="copyrights">

            <div class="container clearfix">

                <div class="col_half">
                    Copyrights &copy; 2017 Tüm Hakları Saklıdır.<br>
                </div>

                <div class="col_half col_last tright">
                    <div class="fright clearfix">
                        <a href="https://www.facebook.com/siamtur" class="social-icon si-small si-borderless si-facebook">
                            <i class="icon-facebook"></i>
                            <i class="icon-facebook"></i>
                        </a>

                        <a href="https://twitter.com/siamtur" class="social-icon si-small si-borderless si-twitter">
                            <i class="icon-twitter"></i>
                            <i class="icon-twitter"></i>
                        </a>

                        <a href="https://www.instagram.com/siamturizm/" class="social-icon si-small si-borderless si-instagram">
                            <i class="icon-instagram"></i>
                            <i class="icon-instagram"></i>
                        </a>

                        <a href="https://plus.google.com/u/0/108439142912433175758/posts" class="social-icon si-small si-borderless si-gplus">
                            <i class="icon-gplus"></i>
                            <i class="icon-gplus"></i>
                        </a>
                    </div>

                    <div class="clear"></div>

                    <i class="icon-envelope2"></i> info@siamtur.com <span class="middot">&middot;</span> <i class="icon-headphones"></i> 444 3 695
                </div>

            </div>

        </div><!-- #copyrights end -->

    </footer><!-- #footer end -->

</div><!-- #wrapper end -->

<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

<!-- External JavaScripts
============================================= -->

<script type="text/javascript" src="/js/template1/plugins.js"></script>

<!-- Travel Demo Specific Script -->
<script type="text/javascript" src="/demos/travel/js/datepicker.js"></script>
<!-- / -->

<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyBeSHmI1J4Cg0aGVTjD2U9-avXFaPx50oo"></script>
<script type="text/javascript" src="/js/template1/jquery.gmap.js"></script>

<!-- Footer Scripts
============================================= -->
<script type="text/javascript" src="/js/template1/functions.js"></script>

<script type="text/javascript">

    $(function() {
        $('.travel-date-group').datepicker({
            autoclose: true,
            startDate: "today",
            format: 'dd/mm/yyyy'
        });
    });

    jQuery(window).load(function(){

        jQuery('#popular-dest-map').gMap({
            address: 'Vietnam',
            maptype: 'ROADMAP',
            zoom: 5,
            markers: [
                {
                    address: "Kamboçya",
                    icon: {
                        image: "/images/icons/map-icon-red.png",
                        iconsize: [32, 39],
                        iconanchor: [16,36]
                    }
                },
                {
                    address: "Tayland",
                    icon: {
                        image: "/images/icons/map-icon-red.png",
                        iconsize: [32, 39],
                        iconanchor: [16, 36]
                    }
                },
                {
                    address: "Laos",
                    icon: {
                        image: "/images/icons/map-icon-red.png",
                        iconsize: [32, 39],
                        iconanchor: [16, 36]
                    }
                },
                {
                    address: "Vietnam",
                    icon: {
                        image: "/images/icons/map-icon-red.png",
                        iconsize: [32, 39],
                        iconanchor: [16, 36]
                    }
                }
            ],
            doubleclickzoom: true,
            controls: {
                panControl: false,
                zoomControl: true,
                mapTypeControl: false,
                scaleControl: false,
                streetViewControl: false,
                overviewMapControl: false
            },
            styles: [{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#e0efef"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"hue":"#1900ff"},{"color":"#c0e8e8"}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":100},{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"visibility":"on"},{"lightness":700}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#7dcdcd"}]}]
        });
    });

</script>

</body>
</html>