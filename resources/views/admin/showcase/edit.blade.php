@extends('admin.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- ALERT -->
	@if (Session::has('flash_notification.message'))
		<div class="alert alert-{{ Session::get('flash_notification.level') }}">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			{{ Session::get('flash_notification.message') }}
		</div>
	@endif
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Vitrin
			<small>Vitrin İçeriği</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="/admin"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
			<li><a href="/admin/showcase"><i class="fa fa-dashboard active"></i> Vitrin</a></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<form action="/admin/showcase/update" method="POST" enctype="multipart/form-data">
				{!! csrf_field() !!}
				<input type="hidden" value="{{ $showcase->id }}" name="showcase_id" enctype="multipart/form-data">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">{{ $showcase->header }} Düzenle</h3>
					</div><!-- /.box-header -->
					<!-- form start -->
					<div class="box-body">
						<div class="form-group">
							<label for="inputShowcaseImage">Vitrin Resmi</label>
							<input type="hidden" name="id" value="{{ $showcase->id }}">
							<input type="file" class="form-control" id="inputShowcaseImage" name="image">
						</div>
						<div class="form-group">
							<label for="inputShowcaseAdi">Vitrin Başlığı</label>
							<input type="text" class="form-control" id="inputShowcaseAdi" name="header" placeholder="Vitrin Adı Giriniz" value="{{$showcase->header}}">
						</div>
						<div class="form-group">
							<label for="inputSayfaKeyword">Anahtar Kelime (Meta Keyword)</label>
							<textarea class="form-control" id="inputSayfaKeyword" name="keyword" placeholder="Örneğin: tatil, seyahat, gezi vb.">{{ $showcase->keyword }}</textarea>
						</div>
						<div class="form-group">
							<label for="inputSayfaAciklama">Sayfa Açıklama (Meta Description)</label>
							<textarea draggable="false" class="form-control" id="inputSayfaAciklama" name="description" placeholder="Sayfa Açıklaması Giriniz">{{ $showcase->description }}</textarea>
						</div>
						<div class="form-group">
							<label for="inputVitrinOnceligi">Vitrin Önceliği</label>
							<input type="text" class="form-control" id="inputVitrinOnceligi" name="priority" placeholder="Boş Bırakırsanız otomatik atanacaktır." value="{{ $showcase->priority }}">
						</div>
						<div class="form-group">
							<label for="inputKategoriOnceligi">Galeri Resmi</label><br>
							Coming Soon...
						</div>
						<div class="form-group">
							<label for="inputVitrinContent">Vitrin İçeriği</label><br>
							<textarea class="form-control product-text" id="inputVitrinContent" name="content"> {{ $showcase->content }}</textarea>
						</div>
					</div><!-- /.box-body -->

					<div class="box-footer">
						<button type="submit" class="btn btn-primary">Kaydet</button>
					</div>
				</div><!-- /.box -->
				</form>
			</div>
		</div><!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection