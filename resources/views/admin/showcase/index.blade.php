@extends('admin.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Vitrin
			<small>Vitrin Listesi</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="/admin"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
			<li><a href="/admin/showcase"><i class="fa fa-dashboard active"></i> Vitrin</a></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Anasayfadaki vitrin alanını buradan değiştirebilirsiniz.</h3>
						</div><!-- /.box-header -->
					</div><!-- /.box -->
				</div>
			</div><!-- /.col -->
		</div><!-- /.row -->
		<div class="col-md-12">
			@foreach($showcases as $showcase)
			<div class="col-md-3 cp-portfolio-item">
				<div class="portfolio-image">
					<a href="/admin/showcase/edit/{{ $showcase->id }}">
						<img src="{{ $showcase->bgimage }}" alt="{{ $showcase->header }}" class="cp-portfolio-img">
						<div class="cp-portfolio-overlay">
							<div class="cp-portfolio-desc">
								<h3>{{ $showcase->header }}</h3>
							</div>
						</div>
					</a>
				</div>
			</div>
			@endforeach
		</div>
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->
<div class="modal modal-success fade" id="modalIlkMesaj" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<form role="form">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Kategori Ekle</h4>
				</div>
				<div class="modal-body">
					<!-- general form elements -->
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title">Kategori Ekle</h3>
						</div><!-- /.box-header -->
						<!-- form start -->
						<form role="form">
							<div class="box-body">
								<div class="form-group">
									<label for="inputKategoriAdi">Kategori Adı</label>
									<input type="text" class="form-control" id="inputKategoriAdi" placeholder="Kategori Adı Giriniz">
								</div>
								<div class="form-group">
									<label for="inputKategoriTuru">Kategori Türü</label>
									<select class="form-control" id="inputKategoriTuru">
										<option value="page">Sayfa Kategori</option>
										<option value="blog">Blog Kategori</option>
										<option value="gallery">Galeri Kategori</option>
									</select>
								</div>
								<div class="form-group">
									<label for="inputKategoriAciklama">Kategori Açıklama</label>
									<textarea type="text" class="form-control" id="inputKategoriAcklama" placeholder="Kategori Adı Giriniz">
									</textarea>
								</div>
								<div class="form-group">
									<label for="exampleInputPassword1">Password</label>
									<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
								</div>
								<div class="form-group">
									<label for="exampleInputFile">File input</label>
									<input type="file" id="exampleInputFile">
									<p class="help-block">Example block-level help text here.</p>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox"> Check me out
									</label>
								</div>
							</div><!-- /.box-body -->

							<div class="box-footer">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</form>
					</div><!-- /.box -->
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
				</div>
			</div><!-- /.modal-content -->
		</form>
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection