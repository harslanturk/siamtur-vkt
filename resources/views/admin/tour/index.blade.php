@extends('admin.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Turlar
			<small>Tur Listeleri</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="/admin"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
			<li><a href="/admin/tour"><i class="fa fa-dashboard active"></i> Tüm Turlar</a></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="col-xs-12">
					<form action="/admin/tour/edit" method="POST">
						{!! csrf_field() !!}
						<input type="hidden" name="cat_id" value="2">
						<div class="box box-primary">
							<div class="box-header">
								<h3 class="box-title">Anasayfada slider üzerindeki alanda olmasını istediğiniz turları seçebilirsiniz.</h3>
							</div><!-- /.box-header -->
							<div class="box-body">
								<div class="col-md-5">
									<div class="box box-primary">
										<div class="box-header">
											<h3 class="box-title">Tüm Turlar</h3>
										</div><!-- /.box-header -->
										<div class="box-body">
											<select name="slider-turlari-ekle[]" multiple class="form-control" style="height:300px;">
												@foreach($allTours as $allTour)
													@foreach($selectCat2Tours as $selectCat2Tour)
														@if($allTour->id == $selectCat2Tour->tur_id)
														<?php $kont = "1"; ?>
														@endif
													@endforeach
													@if($kont!="1")
														<option value="{{ $allTour->id }}">{{ $allTour->id }} - {{ $allTour->tur_ad }}</option>
													@endif
														<?php $kont = "0"; ?>
												@endforeach
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-top:130px;">
									<input type="submit" name="slider-turlari-button" class="btn btn-block btn-success btn-lg" value="Ekle">
									<input type="submit" name="slider-turlari-button" class="btn btn-block btn-danger btn-lg" value="Çıkar">
								</div>
								<div class="col-md-5">
									<div class="box box-primary">
										<div class="box-header">
											<h3 class="box-title">Seçilen Turlar</h3>
										</div><!-- /.box-header -->
										<div class="box-body">
											<select name="slider-turlari-cikar[]" multiple class="form-control" style="height:300px;">
												@foreach($selectCat2Tours as $selectCat2Tour)
													@foreach($allTours as $allTour)
														@if($allTour->id == $selectCat2Tour->tur_id)
															<option value="{{ $allTour->id }}">{{ $allTour->id }} - {{ $allTour->tur_ad }}</option>
														@endif
													@endforeach
												@endforeach
											</select>
										</div>
									</div>
								</div>
							</div>
						</div><!-- /.box -->
					</form>
					<form action="/admin/tour/edit" method="POST">
						{!! csrf_field() !!}
						<input type="hidden" name="cat_id" value="3">
						<div class="box box-primary">
							<div class="box-header">
								<h3 class="box-title">Anasayfada popüler turlar alanında olmasını istediğiniz turları seçebilirsiniz. 4 tur seçmenizi öneriyoruz.</h3>
							</div><!-- /.box-header -->
							<div class="box-body">
								<div class="col-md-5">
									<div class="box box-primary">
										<div class="box-header">
											<h3 class="box-title">Tüm Turlar</h3>
										</div><!-- /.box-header -->
										<div class="box-body">
											<select name="slider-turlari-ekle[]" multiple class="form-control" style="height:300px;">
												@foreach($allTours as $allTour)
													@foreach($selectCat3Tours as $selectCat3Tour)
														@if($allTour->id == $selectCat3Tour->tur_id)
														<?php $kont = "1"; ?>
														@endif
													@endforeach
													@if($kont!="1")
														<option value="{{ $allTour->id }}">{{ $allTour->id }} - {{ $allTour->tur_ad }}</option>
													@endif
														<?php $kont = "0"; ?>
												@endforeach
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-top:130px;">
									<input type="submit" name="slider-turlari-button" class="btn btn-block btn-success btn-lg" value="Ekle">
									<input type="submit" name="slider-turlari-button" class="btn btn-block btn-danger btn-lg" value="Çıkar">
								</div>
								<div class="col-md-5">
									<div class="box box-primary">
										<div class="box-header">
											<h3 class="box-title">Seçilen Turlar</h3>
										</div><!-- /.box-header -->
										<div class="box-body">
											<select name="slider-turlari-cikar[]" multiple class="form-control" style="height:300px;">
												@foreach($selectCat3Tours as $selectCat3Tour)
													@foreach($allTours as $allTour)
														@if($allTour->id == $selectCat3Tour->tur_id)
															<option value="{{ $allTour->id }}">{{ $allTour->id }} - {{ $allTour->tur_ad }}</option>
														@endif
													@endforeach
												@endforeach
											</select>
										</div>
									</div>
								</div>
							</div>
						</div><!-- /.box -->
					</form>
				</div>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection