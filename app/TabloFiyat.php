<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TabloFiyat extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'fiyat';
    protected $fillable = [
        'tur_kod', 'kategori', 'otel', 'oda', 'yemek', 'transfer', 'giristarih', 'tariharaligi', 'fiyat',
    ];
}
