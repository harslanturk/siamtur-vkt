<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Showcase extends Model
{
    protected $table="showcase";
    protected $fillable = [
        'header','content','bgimage','slug','keyword','description','status','priority',
    ];
}
