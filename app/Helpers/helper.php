<?php
/**
 * Created by PhpStorm.
 * User: Hasan
 * Date: 17.10.2016
 * Time: 14:35
 */
namespace App\Helpers;
use App\User;
use Session;
use App\Modules;
use Auth;
use App\UserDelegation;



class Helper
{
    public static function shout($url)
    {
        $url = explode('/', $url);
        $modul = '/'.$url['0'].'/'.$url['1'];

        $id=Modules::where('url',$modul)->select('name')->first()->name;
        $sess=Session::get($id );
        return $sess;
    }
    public static function sessionReload()
    {
        $del_id=Auth::user()->delegation_id;
        $delegation=UserDelegation::where('id',$del_id)->select('auth')->first();

        $json=json_decode($delegation->auth);


        foreach ($json as $key => $str) {
            $name = Modules::where('id', $key)->select('name')->first()->name;
            $read = substr($str, 0, 1);
            $add = substr($str, 1, 1);
            $update = substr($str, 2, 1);
            $delete = substr($str, 3, 1);

            $data = array(
                'r' => $read,
                'a' => $add,
                'u' => $update,
                'd' => $delete,
            );
            Session::put($name, $data);
        }
    }
    public static function MonthNameConverter($oldMonth)
    {
        $search  = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
        $replace = array('OCAK', 'ŞUBAT', 'MART', 'NİSAN', 'MAYIS', 'HAZİRAN', 'TEMMUZ', 'AĞUSTOS', 'EYLÜL', 'EKİM', 'KASIM'    , 'ARALIK');
        $newMonth=str_replace($search, $replace, $oldMonth);
        return $newMonth;
    }
    public static function TrConvert($keyword)
    {
        $tr = array('ş','Ş','ı','İ','ğ','Ğ','ü','Ü','ö','Ö','Ç','ç');
        $eng = array('s','s','i','i','g','g','u','u','o','o','c','c');
        $keyword = str_replace($tr,$eng,$keyword);
        $keyword = strtolower($keyword);
        $keyword = preg_replace('/&.+?;/', '', $keyword);
        $keyword = preg_replace('/[^%a-z0-9 _-]/', '', $keyword);

        /*$keyword = preg_replace('/s+/', '-', $keyword);
        $keyword = preg_replace('|-+|', '-', $keyword);*/
        $keyword = trim($keyword, '-');
        $keyword = str_slug($keyword);

        return $keyword;
    }

    public static function DateConvertTurkishDMY($dt)
    {
        $aylar = array( 
            'January'    =>    'Ocak', 
            'February'    =>    'Şubat', 
            'March'        =>    'Mart', 
            'April'        =>    'Nisan', 
            'May'        =>    'Mayıs', 
            'June'        =>    'Haziran', 
            'July'        =>    'Temmuz', 
            'August'    =>    'Ağustos', 
            'September'    =>    'Eylül', 
            'October'    =>    'Ekim', 
            'November'    =>    'Kasım', 
            'December'    =>    'Aralık', 
            'Monday'    =>    'Pazartesi', 
            'Tuesday'    =>    'Salı', 
            'Wednesday'    =>    'Çarşamba', 
            'Thursday'    =>    'Perşembe', 
            'Friday'    =>    'Cuma', 
            'Saturday'    =>    'Cumartesi', 
            'Sunday'    =>    'Pazar', 
        );
        if($dt == "0000-00-00" || $dt == "" || $dt == NULL)
        {
            $data = "Her Gün Hareketli";
        }
        else{
            $data = date_format (date_create ($dt), 'd F Y');
            $data = strtr($data, $aylar); 
        }  
        return $data;
    }
}