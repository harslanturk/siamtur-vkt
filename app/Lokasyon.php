<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lokasyon extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'lokasyon';
    protected $fillable = [
        'tur_id', 'tayland', 'tayland_sira', 'vietnam', 'vietnam_sira', 'kambocya', 'kambocya_sira', 'singapur', 'singapur_sira', 'malezya', 'malezya_sira', 'endonezya', 'endonezya_sira', 'maldivler', 'maldivler_sira',
        'seyseller', 'seyseller_sira', 'mauritius', 'mauritius_sira',  'laos', 'laos_sira',  'myanmar', 'myanmar_sira', 'hindistan', 'hindistan_sira', 'srilanka', 'srilanka_sira', 'bae', 'bae_sira',
        'hongkong', 'hongkong_sira', 'filipinler', 'filipinler_sira', 'japonya', 'japonya_sira', 'guneykore', 'guneykore_sira', 'zanzibar', 'zanzibar_sira', 'nepal', 'nepal_sira', 'sira',
    ];
}
