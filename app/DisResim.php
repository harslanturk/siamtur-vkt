<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DisResim extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'atur_resim';
    protected $fillable = [
        'tur_no', 'resim',
    ];
}
