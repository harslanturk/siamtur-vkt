<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeziProgrami extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'gezi_programi';
    protected $fillable = [
        'tur_kod', 'icerik',
    ];
}
