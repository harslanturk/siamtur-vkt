<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TumTurlar extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'tum_turlar';
    protected $fillable = [
        'tur_ad', 'htarihi', 'tur_turu', 'gun', 'gece', 'ana_fiyat', 'havayolu', 'ic_detay', 'alt_detay', 'vize', 'erken', 'sira', 'yol', 'tur_icdetay', 'durum', 'tur_bit', 'fiyat_devam', 'des_galeri_durum',
        'para_birimi',  'tur_ad_gor', 'tur_turu_kod',  'kisi_sayisi', 'satin_al', 'vitrin', 'vitrin_sira', 'erkentarih', 'point', 'turfiyat', 'turekstra',
    ];

    public function getTourDisResim()
    {
        return DisResim::where('tur_no', $this->id)->first()->resim;
    }
}
