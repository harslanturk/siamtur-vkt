<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenelSartlar extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'genel_sartlar';
    protected $fillable = [
        'tur_kod', 'icerik',
    ];
}
