<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
    protected $table="tour";
    protected $fillable = [
        'tur_id','cat_id','status','priority',
    ];
}
