<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FotoGaleri extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'foto_galeri';
    protected $fillable = [
        'tur_no', 'resim',
    ];
}
