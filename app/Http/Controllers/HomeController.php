<?php

namespace App\Http\Controllers;


use App\Http\Requests;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use DB;
use Session;
use App\UserDelegation;
use App\Helpers\Helper;
use App\Modules;
use App\Page;
use App\Blog;
use App\Seller;
use App\Service;
use App\Slider;
use App\OurClients;
use App\TumTurlar;
use App\Lokasyon;
use App\Showcase;
use App\Tour;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*public function __construct()
    {
        $this->middleware('auth');
    }*/

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
        
     

    public function index()
    {

        $hak=Page::where('status','1')->get();
        $blog=Blog::where('status','1')->get();        
        $sliders=Slider::where('status','1')->orderBy('priority','asc')->get();
        //$showcase=Showcase::where('status','1')->orderBy('priority','asc')->get();
        /*$selectCat2Tours = Tour::where('cat_id', '2')->get();
        foreach($selectCat2Tours as $key => $value)
        {
            //$data2Tours[$key] = TumTurlar::where('id', $value->tur_id)->first();
            $data2Tours[$key] = DB::table('siamtur_db.tum_turlar')
            ->join('siamtur_db.atur_resim', 'siamtur_db.atur_resim.tur_no', '=', 'siamtur_db.tum_turlar.id')
            ->where('siamtur_db.tum_turlar.id', $value->tur_id)
            ->select('siamtur_db.tum_turlar.*','siamtur_db.atur_resim.resim as resim')
            ->first();
        }*/
        $selectCat3Tours = Tour::where('cat_id', '3')->get();
        foreach($selectCat3Tours as $key => $value)
        {
            //$data3Tours[$key] = TumTurlar::where('id', $value->tur_id)->first();
            $data3Tours[$key] = DB::table('siamtur_db.tum_turlar')
                ->join('siamtur_db.atur_resim', 'siamtur_db.atur_resim.tur_no', '=', 'siamtur_db.tum_turlar.id')
                ->join('siamtur_db.seo', 'siamtur_db.seo.tur_id', '=', 'siamtur_db.tum_turlar.id')
                ->where('siamtur_db.tum_turlar.id', $value->tur_id)
                ->select('siamtur_db.tum_turlar.*','siamtur_db.atur_resim.resim as resim','siamtur_db.seo.link as seo_link','siamtur_db.seo.des as seo_des','siamtur_db.seo.keywords as seo_keywords','siamtur_db.seo.sayfa_ad as seo_sayfa_ad')
                ->first();
        }

        $showcases = Page::where('page_cat', 'LIKE', '%"9"%')->get();
        $filtreler = Page::where('page_cat', 'LIKE', '%"8"%')->orderBy('priority')->get();
        foreach($filtreler as $askey => $filtre)
        {
            $selectTours = json_decode($filtre->page_tour);
            foreach($selectTours as $key => $value)
            {
                //echo $value."---";
                //$data2Tours[$key] = TumTurlar::where('id', $value->tur_id)->first();
                $data2Tours[$askey][$key] = DB::table('siamtur_db.tum_turlar')
                ->join('siamtur_db.atur_resim', 'siamtur_db.atur_resim.tur_no', '=', 'siamtur_db.tum_turlar.id')
                ->join('siamtur_db.seo', 'siamtur_db.seo.tur_id', '=', 'siamtur_db.tum_turlar.id')
                ->where('siamtur_db.tum_turlar.id', $value)
                ->select('siamtur_db.tum_turlar.*','siamtur_db.atur_resim.resim as resim','siamtur_db.seo.link as seo_link','siamtur_db.seo.des as seo_des','siamtur_db.seo.keywords as seo_keywords','siamtur_db.seo.sayfa_ad as seo_sayfa_ad')
                ->first();
                $data2Tours[$askey][$key]->slug = $filtre->slug;
                /*echo "<pre>";
                print_r($selectTours);*/
            }            
        }
       /* echo "<pre>";
        print_r($data2Tours);
        die();*/
        return view('template0.home',['hak'=>$hak,'blog'=>$blog,'sliders'=>$sliders,'showcases'=>$showcases,'data2Tours'=>$data2Tours,'data3Tours'=>$data3Tours, 'filtre' => $filtreler]);
    }
    public function showPage($slug){
        $page=Page::where('slug',$slug)->where('status','1')->first();
        if (isset($page)) {
            return view('template0.page',['page'=>$page]);
        }
        else {
            return redirect()->action('HomeController@index');
        }
        
    }

    public function test(){
        $aylar = array( 
    'January'    =>    'Ocak', 
    'February'    =>    'Şubat', 
    'March'        =>    'Mart', 
    'April'        =>    'Nisan', 
    'May'        =>    'Mayıs', 
    'June'        =>    'Haziran', 
    'July'        =>    'Temmuz', 
    'August'    =>    'Ağustos', 
    'September'    =>    'Eylül', 
    'October'    =>    'Ekim', 
    'November'    =>    'Kasım', 
    'December'    =>    'Aralık', 
    'Monday'    =>    'Pazartesi', 
    'Tuesday'    =>    'Salı', 
    'Wednesday'    =>    'Çarşamba', 
    'Thursday'    =>    'Perşembe', 
    'Friday'    =>    'Cuma', 
    'Saturday'    =>    'Cumartesi', 
    'Sunday'    =>    'Pazar', 
);  
echo strtr(date("d F Y"), $aylar);
echo "<br>"; 
echo Helper::DateConvertTurkishDMY("2018-10-15"); 
        /*setlocale(LC_TIME, 'Turkish'); 
        echo Carbon::parse($today)->format('d F Y');*/
    }

    public function web(){
        return view('welcome');
    }
    public function testBot(){
        function siteConnect($site)
        {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $site);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $site = curl_exec($ch);
            curl_close($ch);
            return $site;
        }
        $giris = siteConnect('http://www.eczanebilgileri.com/Eczane_ilce.asp?id=389');
        preg_match_all('@<p class="hs">(.*?)</p>@si',$giris,$test);
        foreach($test[1] as $value)
        {
            //$value = str_replace("Eczane_detay.asp?id=","",$value);
            echo $value."<br>";
        }
        echo "<pre>";
        print_r($test);
        echo "</pre>";
        /*$giris = siteConnect('http://www.eczanebilgileri.com/');
        preg_match_all('@<p class="category"><a href="(.*?)">@si',$giris,$test);*/
        /*foreach($test[1] as $value)
        {
            $value = str_replace("eczane_il.asp?id=","",$value);
            echo $value."<br>";
            $ilceler = siteConnect('http://www.eczanebilgileri.com/eczane_il.asp?id='.$value);
            preg_match_all('@<p class="category"><a href="(.*?)">@si',$ilceler,$ilces);
            foreach($ilces[1] as $ilce)
            {
                $ilce = str_replace("Eczane_ilce.asp?id=","",$ilce);
                echo $ilce."<br>";
            }*/
            /*$asd = str_split($value);;
            echo "<pre>";
            print_r($asd);
            echo "</pre>";*/
        /*}*/


    }
    public function bot(){
        function siteConnect($site)
        {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $site);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $site = curl_exec($ch);
            curl_close($ch);

            return $site;

        }
        function siteConnectImage($site,$image="")
        {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $site);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            if($image!="")
            {
                $dosya = fopen(public_path()."/uploads/places/".$image.".jpg","w");
                curl_setopt($ch, CURLOPT_FILE, $dosya);
            }
            $site = curl_exec($ch);
            curl_close($ch);

            return $dosya;

        }

        $giris = siteConnect('https://www.zomato.com/tr/istanbul/populer-mekanlar');
        preg_match_all('@<div class="res_title zblack bold " title="(.*?)">@si',$giris,$baslik);
        preg_match_all('@<div class="nowrap grey-text fontsize4">(.*?)</div>@si',$giris,$semt);
        preg_match_all('@<a href="(.*?)" class="jumbo_track_collections"@',$giris,$link);
        /*preg_match_all('@<div class="ptop0 pbot0">

                      <a href="(.*?)" class="jumbo_track_collections" data-link-type="restaurant">@si',$giris,$link);*/
        $i=0;
        foreach($link['1'] as $value)
        {
            $canliMuzik = 0;
            $wifi = 0;
            $alcol = 0;
            $editLink = explode("/",$value);
            $linkutf = urlencode($editLink['5']);
            $sonLink = $editLink['0']."//".$editLink['2']."/".$editLink['3']."/".$editLink['4']."/".$linkutf;
            $giris2 = siteConnect($sonLink);
            preg_match_all('@<span tabindex="0" aria-label="(.*?)" class="tel" itemprop="telephone">@si',$giris2,$phone);
            preg_match('@<div class="resinfo-icon">(.*?)</div>@si',$giris2,$address);
            preg_match_all('@<td class="pl10" >(.*?)</td>@si',$giris2,$workHour);
            preg_match('@<span class="subtext">(.*?)</span>@si',$giris2,$alkol);
            preg_match_all('@<div class="res-info-feature-text">(.*?)</div>@si',$giris2,$ozellik);
            preg_match_all('@<a href="'.$value.'" data-link-type="restaurant" class="relative lazy top-res-box-bg pl10 ptop0" data-original="(.*?)"@si',$giris,$imageLink);
            echo $sonLink."<br>";
            echo"<pre>";
            /*print_r($link);*/
            print_r($imageLink);
            echo "</pre>";
            echo $i;
            $image = $imageLink['1']['0'];
            $image=ltrim($image,"(");
            $image=rtrim($image,")");
            $resname = str_slug($baslik['1'][$i]);
            $giris3 = siteConnectImage($image, $resname);
            //$resname = "deneme";

            if(isset($phone['1']['0']))
            {
                $phone = $phone['1']['0'];
            }
            else
            {
                $phone = "";
            }

            foreach($ozellik['1'] as $ozellikValue)
            {
                if($ozellikValue=="Canlı Mü<zik")
                {
                    $canliMuzik="Canlı Müzik";
                }
                elseif($ozellikValue=="Wifi")
                {
                    $wifi="Wireless";
                }
            }

            if(isset($alkol))
            {
                $alcol="Alkol";
            }
            $optionsAll = array(
                'pazartesi' => $workHour['1']['0'],
                'sali' => $workHour['1']['1'],
                'carsamba' => $workHour['1']['2'],
                'persembe' => $workHour['1']['3'],
                'cuma' => $workHour['1']['4'],
                'cumartesi' => $workHour['1']['5'],
                'pazar' => $workHour['1']['6'],
                "crediCard" => '',
                "alkol" => $alcol,
                "canliMuzik" => $canliMuzik ,
                "parkDurumu" => '0',
                "kupon" => '0',
                "tekSandalye" => '0',
                "wifi" => $wifi,
                "evcilHayvan" => '0',
                "cocuklar" => '0'
            );
            $resimpath = "/uploads/places/".$resname;
            $image = array(
                "url" => $resimpath
            );
            $optionsAll = json_encode($optionsAll);
            $image = json_encode($image);

            $places = new Place();
            $places->title = $baslik['1'][$i];
            $places->address = $semt['1'][$i];
            $places->country = "Türkiye";
            $places->city = "İstanbul";
            $places->slug = str_slug($baslik['1'][$i]);
            $places->options = $optionsAll;
            $places->phone = $phone;
            $places->image = $image;
            $places->save();
            $i++;
        }
        /*foreach($baslik['1'] as $value)
        {
            echo $value."<br>";
        }*/
        /*echo $baslik['1']['5'];
        echo "<pre>";
        print_r($baslik);
        print_r($semt);
        echo "</pre>";*/
    }

}
