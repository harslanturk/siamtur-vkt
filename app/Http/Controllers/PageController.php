<?php

namespace App\Http\Controllers;

use App\DisResim;
use App\EkstraTurlar;
use App\FotoGaleri;
use App\GenelSartlar;
use App\GeziProgrami;
use App\TumTurlar;
use Illuminate\Http\Request;
use App\Page;
use DB;
use App\Tour;

use App\Http\Requests;

class PageController extends Controller
{
    public function showTour($tour_ad, $tour_id){
        
        $tour = TumTurlar::where('id', $tour_id)->first();
        $geziProgrami = GeziProgrami::where('tur_kod', $tour_id)->first();
        $genelSartlar = GenelSartlar::where('tur_kod', $tour_id)->first();
        $ekstraTurlar = EkstraTurlar::where('tur_kod', $tour_id)->first();
        $disResim = DisResim::where('tur_no', $tour_id)->first();
        $fotoGaleri = FotoGaleri::where('tur_no', $tour_id)->get();
        $seo = DB::table('siamtur_db.seo')
                ->where('siamtur_db.seo.tur_id', $tour_id)
                ->select('siamtur_db.seo.*')
                ->first();
        $fiyat = DB::table('siamtur_db.fiyat')
        ->where('siamtur_db.fiyat.tur_kod', $tour_id)
        ->select('siamtur_db.fiyat.*')
        ->get();
        $fiyatlar = DB::table('siamtur_db.fiyatlar')
        ->where('siamtur_db.fiyatlar.tur_kod', $tour_id)
        ->select('siamtur_db.fiyatlar.*')
        ->first();
        $price = DB::table('siamtur_db.price')
        ->where('siamtur_db.price.tur_kod', $tour_id)
        ->select('siamtur_db.price.*')
        ->get();
        $havayolu=$tour->havayolu;
        if($havayolu=="1")
        {
            $havayolu="thy";
        }
        else if($havayolu=="2")
        {
            $havayolu="qatar";
        }
        else if($havayolu=="3")
        {
            $havayolu="singapore";
        }
        else if($havayolu=="4")
        {
            $havayolu="emirates";
        }
        else if($havayolu=="6")
        {
            $havayolu="korean";
        }
        else
            $havayolu="malaysia";
        return view('template0.show', ['tour' => $tour, 'geziProgrami' => $geziProgrami, 'genelSartlar' => $genelSartlar, 'ekstraTurlar' => $ekstraTurlar, 'disResim' => $disResim, 'fotoGaleri' => $fotoGaleri, 'havayolu' => $havayolu, 'seo' => $seo, 'fiyat' => $fiyat, 'fiyatlar' => $fiyatlar, 'price' => $price]);
    }
    public function allPage($sayfa){
        $kontSSS = Page::where('slug', $sayfa)->first();
        
        
        if(!empty($kontSSS))
        {
            $filtreler = Page::where('page_cat', 'LIKE', '%"8"%')->orderBy('priority')->get();
            $selectTours = Tour::where('cat_id', '10')->get();
                
            foreach($selectTours as $key => $value)
            {
                //echo "-".$value->tur_id."---";
                //$data2Tours[$key] = TumTurlar::where('id', $value->tur_id)->first();
                $data2Tours[$key] = DB::table('siamtur_db.tum_turlar')
                ->join('siamtur_db.atur_resim', 'siamtur_db.atur_resim.tur_no', '=', 'siamtur_db.tum_turlar.id')
                ->join('siamtur_db.seo', 'siamtur_db.seo.tur_id', '=', 'siamtur_db.tum_turlar.id')
                ->where('siamtur_db.tum_turlar.id', $value->tur_id)
                ->select('siamtur_db.tum_turlar.*','siamtur_db.atur_resim.resim as resim','siamtur_db.seo.link as seo_link','siamtur_db.seo.des as seo_des','siamtur_db.seo.keywords as seo_keywords','siamtur_db.seo.sayfa_ad as seo_sayfa_ad')
                ->first();
                /*echo "<pre>";
                print_r($selectTours);*/
            }            
            //$filtreler = Page::where('page_cat', 'LIKE', '%"8"%')->orderBy('priority')->get();
            return view('template0.sss', ['data' => $kontSSS, 'filtre' => $filtreler, 'data2Tours'=>$data2Tours]);
        }
        else
        {
            return view('template0.page');
        }
    }
    public function allTour(){
        return view('template0.all-tour');
    }
}
