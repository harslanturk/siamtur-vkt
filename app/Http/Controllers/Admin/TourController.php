<?php

namespace App\Http\Controllers\Admin;

use App\TumTurlar;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Tour;

class TourController extends Controller
{
    public function index(){
        $allTours = TumTurlar::where('durum', 'on')->get();
        $selectCat2Tours = Tour::where('cat_id', '2')->get();
        $selectCat3Tours = Tour::where('cat_id', '3')->get();
        return view('admin.tour.index', ['allTours' => $allTours, 'selectCat2Tours' => $selectCat2Tours, 'selectCat3Tours' => $selectCat3Tours]);
    }
    public function edit(Request $request){
        $data = $request->all();
        /*echo "<pre>";
        print_r($data);
        die();*/
        if($data['slider-turlari-button']=="Ekle")
        {
            $turlar = $data['slider-turlari-ekle'];
            foreach($turlar as $tur)
            {
                //echo $tur."<br>";
                $tour = new Tour();
                $tour->tur_id = $tur;
                $tour->cat_id = $data['cat_id'];
                $tour->status = "1";
                $tour->save();

            }
        }
        elseif($data['slider-turlari-button']=="Çıkar"){
            $turlar = $data['slider-turlari-cikar'];
            foreach($turlar as $tur)
            {
                $tour = Tour::where('tur_id',$tur)->first();
                $tour->delete();
            }
        }
        return redirect()->back();

    }
}
