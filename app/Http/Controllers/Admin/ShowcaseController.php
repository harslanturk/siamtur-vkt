<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Showcase;
use DB;
use Auth;
use App\Helpers\Helper;
use Session;
use Laracasts\Flash\Flash;

class ShowcaseController extends Controller
{
    public function index(){
        $showcase = Showcase::where('status', 1)->get();
        return view('admin.showcase.index', ['showcases' => $showcase]);
    }
    public function edit($id){
        $showcase= Showcase::where('id', $id)->first();
        return view('admin.showcase.edit', ['showcase' => $showcase]);
    }
    public function update(Request $request)
    {
        if($request->file('image') != null) {
            $path = base_path() . '/public/img/showcase';

            $imageTempName = $request->file('image')->getPathname();
            $current_time = time();
            $imageName = $current_time."_".$request->file('image')->getClientOriginalName();

            $request->file('image')->move($path , $imageName);
            $newresim = '/img/showcase/'.$imageName;
            /*echo $newresim;
            die();*/
        }
        else{
            $spshowcase = Showcase::where('id', $request->input('id'))->first();
            $newresim = $spshowcase->image;
        }
        $slug = str_slug($request->input('header'));
        $showcase = Showcase::find($request->input('id'));
        $showcase->header = $request->input('header');
        $showcase->content = $request->input('content');
        $showcase->priority = $request->input('priority');
        $showcase->bgimage = $newresim;
        $showcase->slug = $slug;
        $showcase->keyword = $request->input('keyword');
        $showcase->description = $request->input('description');
        $showcase->status = "1";
        $showcase->save();
        Flash::message('Vitrin başarılı bir şekilde güncellendi.','success');
        return redirect()->back();
    }
}
