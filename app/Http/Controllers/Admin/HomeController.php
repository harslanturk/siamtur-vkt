<?php

namespace App\Http\Controllers\Admin;

use App\Patient;
use App\Report;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Helpers\Helper;
use Session;
use App\User;
use App\Page;
use App\Reference;

class HomeController extends Controller
{
    public function __construct(Request $request)
    {
        Helper::sessionReload();

    }
    public function index()
    {
        $pages = Page::where('status', 1)->count();
        $references = Reference::where('status', 1)->count();
        $users = User::where('status', 1)->count();

        return view('admin.home',['pages' => $pages,'references' => $references,'users' => $users]);

        //return view('admin.home');
    }
}
