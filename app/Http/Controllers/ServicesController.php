<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use DB;
use Session;
use App\UserDelegation;
use App\Helpers\Helper;
use App\Modules;
use App\Page;
use App\Service;

class ServicesController extends Controller
{
    public function index(){
    	$services=Service::where('status','1')->orderBy('priority','asc')->get();
    	$pageService=Page::where('status','1')->where('slug','neler-yapiyoruz')->get();
    	return view('template0.services',['services'=>$services,'pageService'=>$pageService]);
    }

	public function metalEnjeksiyon(){
		$pageService=Page::where('status','1')->where('slug','neler-yapiyoruz')->get();
		return view('template1.metal_enjeksiyon', ['pageService'=>$pageService]);
	}

	public function yedekParca(){
		return view('template1.yedek_parca');
	}

	public function urunDetay30Ton($id=""){
		return view('template1.urun_detay_30ton', ['urun'=>$id]);
	}

	public function urunDetay120Ton($id=""){
		return view('template1.urun_detay_120ton', ['urun'=>$id]);
	}

	public function urunDetay200Ton($id=""){
		return view('template1.urun_detay_200ton', ['urun'=>$id]);
	}

	public function urunDetay400Ton($id=""){
		return view('template1.urun_detay_400ton', ['urun'=>$id]);
	}

	public function servis(){
		return view('template1.servis');
	}
}
