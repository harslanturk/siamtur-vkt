<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EkstraTurlar extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'ekstra_turlar';
    protected $fillable = [
        'tur_kod', 'icerik',
    ];
}
